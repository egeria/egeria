/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const EError = require('@egeria/error')

const trace = (x) => {
  let fn
  try {
    let po = require('prettyoutput')
    fn = (x) => console.log(po(x))
  } catch (e) {
    fn = R.pipe(R.toString, R.concat('TRACE: '), console.log)
  }
  fn(x)
  return x
}

function wrapLog (state, message, origin, data) {
  let prefix = state.get('prefix')
  let outbox = state.get('outbox')
  let level = origin.level
  let wrappedMessage = EError.of(prefix, message, origin.message, data)
  outbox.push({ level: level, message: wrappedMessage })
}

function log (state, level, message, origin, data) {
  let prefix = state.get('prefix')
  let outbox = state.get('outbox')
  let wrapper = EError.of(prefix, message, origin, data)
  outbox.push({ level: level, message: wrapper })
}

function err (prefix, message, origin, data) {
  return new EError(prefix, message, origin, data)
}

function setupLogging () {
  return {
    logSilly: (state, message, origin, data) => log(state, 'silly', message, origin, data),
    logDebug: (state, message, origin, data) => log(state, 'debug', message, origin, data),
    logInfo: (state, message, origin, data) => log(state, 'info', message, origin, data),
    logWarning: (state, message, origin, data) => log(state, 'warn', message, origin, data),
    logError: (state, message, origin, data) => log(state, 'error', message, origin, data)
  }
}

const dateFormat = R.curry((format, date) => {
  return require('moment')(date).format(format)
})

const illegalRe = /[/?<>\\:*|":]/g
const controlRe = /[\x00-\x1f\x80-\x9f]/g // eslint-disable-line
const reservedRe = /^\.+$/
const windowsReservedRe = /^(con|prn|aux|nul|com[0-9]|lpt[0-9])(\..*)?$/i

const sanitize = (input, replacement) => {
  let sanitized = input
    .replace(illegalRe, replacement)
    .replace(controlRe, replacement)
    .replace(reservedRe, replacement)
    .replace(windowsReservedRe, replacement)
  return sanitized
}

const sanitizeFilename = (input, replacement) => {
  let output = sanitize(input, replacement)
  if (replacement === '') {
    return output
  }
  return sanitize(output, '')
}

const findByProp = R.curry((prop, filterable, value) => R.pipe(R.propEq(prop), R.find(R.__, filterable))(value))

const mergeResultOf = R.curry((fn, obj) => R.converge(R.merge, [R.identity, fn])(obj))

const filterByProp = R.curry((key, values, list) => R.pipe(
  R.ifElse(
    R.either(
      () => R.length(values) === 0,
      R.pipe(R.length, R.equals(0))
    ),
    R.always([]), // we ran out of possible values, return empty: no match
    R.pipe(
      R.groupBy(R.prop(key)),
      R.ifElse(
        R.pipe(R.prop(values[0]), R.isNil),
        () => filterByProp(key, R.tail(values), list), // exclude value
        R.prop(values[0]) // accept items having this value
      )
    )
  )
)(list))

const pad = R.curry((z, width, n) => {
  n = n + ''
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
})

const dataconv = (mask, keys) => R.pipe(
  R.of,
  R.ap(R.map(R.path, mask)),
  R.zipObj(keys)
)

const copyTo = R.curry((dest, sourceFn, data) => R.converge(R.assoc, [
  R.always(dest),
  sourceFn,
  R.identity
])(data))

const copyToPath = R.curry((dest, sourceFn, data) => R.converge(R.assocPath, [
  R.always(dest),
  sourceFn,
  R.identity
])(data))

const collapse = R.curry((k, obj) => {
  let o = {}
  let ks = R.keys(obj)
  for (let kk of ks) {
    let v = obj[kk]
    if (R.is(Object, v) && !R.is(Date, v)) {
      o = R.merge(o, collapse(k + kk + ':', v))
    } else if (!R.is(Function, v)) {
      o[k + kk] = v
    }
  }
  return o
})

const mkRegExp = (exp) => {
  if (R.is(RegExp, exp)) {
    return exp
  }
  if (R.is(String, exp) || R.is(Number, exp)) {
    return new RegExp(exp)
  }
  if (R.is(Array, exp)) {
    let actualExp
    try {
      actualExp = new RegExp(exp[0], exp[1])
    } catch (e) {
      throw new Error('Invalid regular expression: ' + e.message)
    }
    return actualExp
  }
}

const isMissing = R.either(R.isNil, R.isEmpty)

module.exports = {
  trace,
  err,
  log,
  wrapLog,
  setupLogging,
  dateFormat,
  sanitizeFilename,
  pad,
  findByProp,
  filterByProp,
  mergeResultOf,
  dataconv,
  copyTo,
  copyToPath,
  collapse,
  mkRegExp,
  isMissing
}
