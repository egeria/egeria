/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const Google = require('googleapis').google
const M = require('moment')
const T = require('@egeria/tools')

const prefix = 'PLUGIN youtube |'

const rethrow = R.curry((action, e) => {
  throw T.err(prefix, action, e)
})

const err = (origin, action, data) => T.err(prefix, action, origin, data)

async function prequest (client, enqueue, method, args) {
  var action = 'While calling API method "' + method + '" with arguments ' + JSON.stringify(args)
  return enqueue(() => new Promise((resolve, reject) => client[method].list(args, (e, res) => {
    if (R.isNil(e)) {
      resolve(res.data)
    } else {
      reject(err(e, action, { method, args }))
    }
  })))
}

const apiCall = R.curry(async (configuration, client, enqueue, args) => {
  var _args = R.clone(args)
  if (R.defaultTo(50, args.params.maxResults) >= 50) {
    args.params.maxResults = 50
  }
  let callArgs = R.merge(configuration.params, args.params)
  let data = await prequest(client, enqueue, configuration.method, callArgs)
  data.items = R.map((item) => {
    // merge the snippet properties with the item's, removing the snippet
    let i = R.merge(R.dissoc('snippet', item), R.prop('snippet', item))
    // if it's a video fix the id and add a link to its page
    let videoId = R.defaultTo(R.path(['id', 'videoId'], i), R.path(['resourceId', 'videoId'], i))
    if (!R.isNil(videoId)) {
      if (item.snippet.liveBroadcastContent !== 'none') {
        return null
      }
      i.link = 'https://www.youtube.com/watch?v=' + videoId
      i.id = videoId
      i = R.dissoc('resourceId', i)
    }
    // convert every date to native JS date objects
    i = R.mapObjIndexed(R.ifElse(
      R.test(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z/),
      (d) => M(d, M.ISO_8601).toDate(),
      R.identity
    ), i)
    // collapse keys such as "link" to "youtube:link" and paths like "one.two" to "youtube:one:two"
    i = T.collapse(configuration.prefix, i)
    return i
  }, data.items)
  // concatenate any previously fetched items
  data.items = R.concat(R.defaultTo([], args.accum), R.reject(R.isNil, data.items))
  // set up the next iteration
  _args.params.pageToken = data.nextPageToken
  _args.params.maxResults -= data.items.length
  // if we got enough items return them, otherwise accumulate them and fetch the next page
  if (R.either(R.isNil, R.isEmpty)(data.nextPageToken) || _args.params.maxResults <= 0) {
    return R.map(R.merge(R.defaultTo({}, args.parent)), data.items)
  } else {
    _args.accum = data.items
    return apiCall(configuration, client, enqueue, _args)
  }
})

const getSubscriptions = apiCall({
  method: 'subscriptions',
  prefix: 'channel:',
  params: {
    part: 'snippet',
    mine: true
  }
})

const getVideos = apiCall({
  method: 'search',
  prefix: 'video:',
  params: {
    part: 'snippet',
    safeSearch: 'none',
    type: 'video',
    order: 'date'
  }
})

const getChannels = apiCall({
  method: 'channels',
  prefix: 'channel:',
  params: {
    part: 'snippet',
    order: 'date'
  }
})

const getPlaylists = apiCall({
  method: 'playlists',
  prefix: 'playlist:',
  params: {
    part: 'snippet',
    order: 'date'
  }
})

const getPlaylistVideos = apiCall({
  method: 'playlistItems',
  prefix: 'video:',
  params: {
    part: 'snippet',
    order: 'date'
  }
})

async function getClient (identity) {
  var googleOAuth2Client = new Google.auth.OAuth2(identity.clientId, identity.clientSecret, 'none')
  googleOAuth2Client.setCredentials({
    refresh_token: identity.refreshToken
  })
  return {
    auth: googleOAuth2Client,
    google: Google.youtube({ version: 'v3', auth: googleOAuth2Client }),
    identity
  }
}

async function getIdentity (client) {
  return client.identity
}

const subscriptions = R.curry(async (api, cfg) => {
  var subaction = 'While fetching the list of subscribed channels'
  let subs
  try {
    subs = await getSubscriptions(api.client.google, api.enqueue, { params: { maxResults: Infinity } })
  } catch (e) {
    rethrow(subaction, e)
  }
  let videoRequests = []
  for (let subscription of subs) {
    let channelId = subscription['channel:resourceId:channelId']
    let action = 'While fetching the list of videos in a channel with id ' + channelId
    let callparams = {
      parent: subscription,
      params: {
        channelId: channelId,
        maxResults: cfg.limit,
        order: R.defaultTo('date', cfg.order)
      }
    }
    videoRequests.push(getVideos(api.client.google, api.enqueue, callparams).catch(rethrow(action)))
  }
  return Promise.all(videoRequests)
})

const playlists = R.curry(async (api, cfg) => {
  let playlistRequests = []
  for (let id of cfg.ids) {
    let action = 'While fetching information about a playlist with id ' + id
    let callparams = {
      params: {
        id: id,
        maxResults: Infinity
      }
    }
    playlistRequests.push(getPlaylists(api.client.google, api.enqueue, callparams).catch(rethrow(action)))
  }
  let playlists = R.flatten(await Promise.all(playlistRequests))
  playlistRequests = null
  let channelRequests = []
  for (let playlist of playlists) {
    let action = 'While fetching data about a channel with id ' + playlist['playlist:channelId']
    let callparams = {
      parent: playlist,
      params: {
        id: playlist['playlist:channelId'],
        maxResults: Infinity
      }
    }
    channelRequests.push(getChannels(api.client.google, api.enqueue, callparams).catch(rethrow(action)))
  }
  playlists = R.flatten(await Promise.all(channelRequests))
  channelRequests = null
  let videoRequests = []
  for (let playlist of playlists) {
    let action = 'While fetching the list of videos in a playlist with id ' + playlist['playlist:id']
    let callparams = {
      parent: playlist,
      params: {
        playlistId: playlist['playlist:id'],
        maxResults: cfg.limit,
        order: R.defaultTo('date', cfg.order)
      }
    }
    videoRequests.push(getPlaylistVideos(api.client.google, api.enqueue, callparams).catch(rethrow(action)))
  }
  let videos = await Promise.all(videoRequests)
  return videos
})

const search = R.curry(async (api, cfg) => {
  let videoRequests = []
  for (let q of cfg.ids) {
    let action = 'While searching for videos using query "' + q + '"'
    let callparams = {
      params: {
        q: q,
        maxResults: cfg.limit,
        order: R.defaultTo('date', cfg.order)
      }
    }
    videoRequests.push(getVideos(api.client.google, api.enqueue, callparams).catch(rethrow(action)))
  }
  let videos = R.flatten(await Promise.all(videoRequests))
  let channelRequests = []
  for (let video of videos) {
    let channelId = video['video:channelId']
    let action = 'While fetching data about a channel with id ' + channelId
    let callparams = {
      parent: video,
      params: {
        id: channelId,
        maxResults: Infinity
      }
    }
    channelRequests.push(getChannels(api.client.google, api.enqueue, callparams).catch(rethrow(action)))
  }
  return Promise.all(channelRequests)
})

const channels = R.curry(async (api, cfg) => {
  let channelRequests = []
  for (let username of cfg.ids) {
    let action = 'While fetching data about channels associated with username ' + username
    let callparams = {
      params: {
        forUsername: username,
        maxResults: Infinity
      }
    }
    channelRequests.push(getChannels(api.client.google, api.enqueue, callparams).catch(rethrow(action)))
  }
  let channels = R.flatten(await Promise.all(channelRequests))
  let videoRequests = []
  for (let channel of channels) {
    let channelId = channel['channel:id']
    let action = 'While fetching the list of videos from a channel with id ' + channelId
    let callparams = {
      parent: channel,
      params: {
        channelId: channelId,
        maxResults: cfg.limit,
        order: R.defaultTo('date', cfg.order)
      }
    }
    videoRequests.push(getVideos(api.client.google, api.enqueue, callparams).catch(rethrow(action)))
  }
  return Promise.all(videoRequests)
})

const modes = { subscriptions, playlists, search, channels }

const action = R.curry(async (api, cfg) => {
  if (cfg.mode !== 'subscriptions' && T.isMissing(cfg.ids)) {
    var e = new Error(`When using mode ${cfg.mode} you must specify a list of ids.`)
    throw err(e, 'While initializing youtube plugin')
  }
  if (R.isNil(cfg.ids) || cfg.mode === 'subscriptions') {
    cfg.ids = ['stub']
  } else {
    cfg.ids = R.flatten(R.of(cfg.ids))
  }
  let data = R.flatten(await modes[cfg.mode](api, cfg))
  return R.map(R.pipe(
    T.copyTo('key', R.pipe(R.prop('video:id'), R.concat('youtube:'))),
    T.collapse('youtube:'),
    T.copyTo('key', R.prop('youtube:key')),
    R.assoc('origin', 'youtube'),
    api.announce
  ), data)
})

const plugins = {
  youtube: {
    type: 'input',
    requires: ['identity', 'schedule'],
    sanity: {
      type: 'object',
      required: ['mode', 'limit'],
      switch: [{
        if: { properties: { mode: { not: { const: 'subscriptions' } } } },
        then: { required: ['ids'] }
      }],
      properties: {
        mode: {
          type: 'string',
          enum: ['subscriptions', 'playlists', 'search', 'channels']
        },
        ids: { type: ['string', 'array'] },
        limit: {
          type: 'number',
          exclusiveMinimum: 0
        },
        sort: {
          type: 'string',
          enum: ['relevance', 'date']
        }
      }
    },
    limits: {
      upstream: 'google',
      concurrency: 4,
      delay: 1,
      timeout: 60
    },
    getClient,
    getIdentity,
    act: action
  }
}

module.exports = plugins
