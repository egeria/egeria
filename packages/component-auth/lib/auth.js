/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

module.exports = function (queue, state) {
  const E = require('simple-encryptor')(state.get('masterPassword'))
  const T = require('@egeria/tools')
  const FS = require('@egeria/fslib')
  const path = require('path')
  const { logSilly, logDebug } = T.setupLogging()
  const err = (message, origin, data) => T.err(prefix, message, origin, data)

  const limits = { upstream: 'storage:diskops', concurrency: 4, delay: 0, timeout: Infinity }
  const foptions = 'utf8'
  const prefix = 'ID manager |'
  state.set('prefix', prefix)

  async function load (identity) {
    logDebug(state, 'Loading identity "' + identity + '"')
    if (state.select('identities', identity).exists()) {
      return state.get('identities', identity)
    }
    let action = 'While loading identity ' + identity
    let directory = state.get('directory')
    let filename = identity + '.encrypted.id'
    let encryptedCredentials
    try {
      encryptedCredentials = await queue.enqueue(limits, () => FS.read(path.resolve(directory, filename), foptions))
    } catch (e) {
      throw err(action, e, { directory, filename })
    }
    let decryptedCredentials = E.decrypt(encryptedCredentials)
    state.select('identities', identity).set(decryptedCredentials)
    logSilly(state, 'Identity "' + identity + '" loaded successfully')
    return decryptedCredentials
  }

  async function store (identity) {
    logDebug(state, 'Saving identity "' + identity.name + '"')
    let action = 'While storing identity ' + identity.name
    state.select('identities', identity.name).set(identity)
    let directory = state.get('directory')
    let filename = identity.name + '.encrypted.id'
    let encryptedCredentials
    try {
      encryptedCredentials = E.encrypt(identity)
      await queue.enqueue(limits, () => FS.save(path.resolve(directory, filename), encryptedCredentials, foptions))
    } catch (e) {
      throw err(action, e, { directory, filename, identity, encryptedCredentials })
    }
    return true
  }

  return { load, store }
}
