/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const FS = require('@egeria/fslib')
const path = require('path')

const rethrow = R.curry((prefix, action, e) => {
  throw T.err(prefix, action, e)
})

const resolvePath = R.curry(R.binary(path.resolve))

async function announceDirectory (dir, recurse, announce) {
  let contents = await FS.readdir(dir)
  let paths = R.map(resolvePath(dir), contents)
  let directories = []
  for (let itemPath of paths) {
    let type
    try {
      type = await FS.stat(itemPath)
    } catch (e) {
      // logError('PLUGIN fileList |', type)
      continue
    }
    if (type.isFile()) {
      let fname = path.basename(itemPath)
      let metadata = T.collapse('file:', R.merge(type, {
        fullPath: itemPath,
        name: fname,
        key: 'file:' + itemPath
      }))
      metadata.key = metadata['file:key']
      metadata.origin = 'fileList'
      announce(metadata)
    } else if (recurse && type.isDirectory()) {
      directories.push(announceDirectory(itemPath, recurse, announce))
    } else {
      // logWarning('PLUGIN fileList |', 'A path was ignored because it wasn\'t either a file or a directory: ' + path)
    }
  }
  if (recurse && directories.length) {
    return Promise.all(directories)
  } else {
    return true
  }
}

async function fileListAct (api, cfg = {}) {
  let dir = cfg.path
  let recurse = R.defaultTo(true, cfg.recurse)
  await announceDirectory(dir, recurse, api)
}

async function fileMoveAct (api, cfg = {}, fact) {
  let oldPath = fact.applyTemplate(cfg.oldPath)
  let oldName = fact.applyTemplate(cfg.oldName)
  let newPath = fact.applyTemplate(cfg.newPath)
  let newName = fact.applyTemplate(cfg.newName)
  let overwrite = cfg.overwrite
  let hasOldName = !R.isNil(cfg.oldName)
  let source
  let destination
  if (hasOldName) {
    source = path.resolve(oldPath, oldName)
  } else {
    source = oldPath
  }
  if (newName === 'preserve') {
    destination = path.resolve(newPath, path.basename(source))
  } else {
    destination = path.resolve(newPath, newName)
  }
  let action = 'While moving ' + source + ' to ' + destination
  try {
    await FS.ensureDir(newPath)
    await FS.move(source, destination, { overwrite })
  } catch (e) {
    rethrow('PLUGIN fileMove |', action, e)
  }
}

const plugins = {
  fileList: {
    type: 'input',
    requires: ['schedule'],
    sanity: {
      type: 'object',
      required: ['path'],
      properties: {
        path: { type: 'string' },
        recurse: { type: 'boolean' }
      }
    },
    limits: {
      upstream: 'localfile',
      concurrency: 2,
      delay: 0,
      timeout: 3600
    },
    act: fileListAct
  },
  fileMove: {
    type: 'output',
    sanity: {
      type: 'object',
      required: ['oldPath', 'newPath', 'newName'],
      properties: {
        oldPath: { type: 'string' },
        oldName: { type: 'string' },
        newPath: { type: 'string' },
        newName: { type: 'string' },
        overwrite: { type: 'boolean' }
      }
    },
    limits: {
      upstream: 'localfile',
      concurrency: 2,
      delay: 0,
      timeout: 3600
    },
    act: fileMoveAct
  }
}

module.exports = plugins
