/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const W = require('@egeria/httplib')
const M = require('moment')

const userAgent = 'Node.js/' + process.version + ' Egeria/Appveyor-plugin/v' + require('../package.json').version
const apiBaseURL = 'https://ci.appveyor.com/api'

async function getProject (token, enqueue, accountName, projectSlug) {
  let headers = {
    'User-Agent': userAgent,
    'Authorization': 'Bearer ' + token,
    'Content-type': 'application/json'
  }
  let parms = {
    headers,
    method: 'get',
    json: true,
    uri: apiBaseURL + '/projects/' + accountName + '/' + projectSlug
  }
  return (await enqueue(() => W.httpreq(parms))).body
}

async function getArtifacts (token, enqueue, jobId) {
  let headers = {
    'User-Agent': userAgent,
    'Authorization': 'Bearer ' + token,
    'Content-type': 'application/json'
  }
  let parms = {
    headers,
    method: 'get',
    json: true,
    uri: apiBaseURL + '/buildjobs/' + jobId + '/artifacts'
  }
  return (await enqueue(() => W.httpreq(parms))).body
}

async function downloadArtifact (token, enqueue, jobId, artifactFileName, destination) {
  let headers = {
    'User-Agent': userAgent,
    'Authorization': 'Bearer ' + token
  }
  let parms = {
    headers,
    method: 'get',
    preserveFilename: 'true',
    uri: apiBaseURL + '/buildjobs/' + jobId + '/artifacts/' + artifactFileName
  }
  return enqueue(() => W.download(parms, destination))
}

async function getClient (identity) {
  return {
    appveyor: {
      getProject: R.curry(getProject)(identity.token),
      getArtifacts: R.curry(getArtifacts)(identity.token),
      downloadArtifact: R.curry(downloadArtifact)(identity.token)
    },
    identity
  }
}

async function getIdentity (client) {
  return client.identity
}

async function artifactsAction (api, cfg) {
  let accountName = cfg.accountName
  let projectSlug = cfg.projectSlug
  let project = await api.client.appveyor.getProject(api.enqueue, accountName, projectSlug)
  let projectMetadata = R.pipe(
    R.dissocPath(['build', 'jobs']),
    R.dissocPath(['project', 'securityDescriptor'])
  )(project)
  projectMetadata.project.created = M(projectMetadata.project.created).toDate()
  projectMetadata.project.updated = M(projectMetadata.project.updated).toDate()
  projectMetadata.build.started = M(projectMetadata.build.started).toDate()
  projectMetadata.build.finished = M(projectMetadata.build.finished).toDate()
  projectMetadata.build.created = M(projectMetadata.build.created).toDate()
  projectMetadata.build.updated = M(projectMetadata.build.updated).toDate()
  for (let job of project.build.jobs) {
    let jobMetadata = { job: job }
    jobMetadata.job.started = M(jobMetadata.job.started).toDate()
    jobMetadata.job.finished = M(jobMetadata.job.finished).toDate()
    jobMetadata.job.created = M(jobMetadata.job.created).toDate()
    jobMetadata.job.updated = M(jobMetadata.job.updated).toDate()
    let artifacts = await api.client.appveyor.getArtifacts(api.enqueue, job.jobId)
    for (let artifact of artifacts) {
      let artifactMetadata = { artifact: artifact }
      let metadata = T.collapse('appveyor:', R.mergeAll([projectMetadata, jobMetadata, artifactMetadata]))
      metadata.key = metadata['appveyor:key'] = R.join(':', ['appveyor', accountName, projectSlug, job.jobId, artifact.fileName])
      metadata.origin = 'appveyorArtifacts'
      api.announce(metadata)
    }
  }
}

async function artifactDownloadAction (api, cfg, fact) {
  let jobId = fact.applyTemplate(R.defaultTo('{{appveyor:job:jobId}}', cfg.jobId))
  let artifactFileName = fact.applyTemplate(R.defaultTo('{{appveyor:artifact:fileName}}', cfg.artifactFilename))
  let destination = cfg.destination
  await api.client.appveyor.downloadArtifact(api.enqueue, jobId, artifactFileName, destination)
  return fact
}

const plugins = {
  appveyorArtifacts: {
    type: 'input',
    requires: ['identity', 'schedule'],
    sanity: {
      type: 'object',
      required: ['identity', 'accountName', 'projectSlug'],
      properties: {
        accountName: { type: 'string' },
        projectSlug: { type: 'string' }
      }
    },
    limits: {
      upstream: 'appveyor',
      concurrency: 4,
      delay: 1,
      timeout: 60
    },
    act: artifactsAction,
    getClient,
    getIdentity
  },
  appveyorDownload: {
    type: 'output',
    requires: ['identity'],
    sanity: {
      required: ['identity', 'destination'],
      properties: {
        jobId: { type: 'string' },
        artifactFileName: { type: 'string' },
        destination: { type: 'string' }
      }
    },
    limits: {
      upstream: 'httpdl',
      concurrency: 2,
      delay: 1,
      timeout: 60
    },
    act: artifactDownloadAction,
    getClient,
    getIdentity
  }
}

module.exports = plugins
