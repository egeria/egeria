/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

module.exports = function (state) {
  const R = require('ramda')
  const T = require('@egeria/tools')
  const pathlib = require('path')

  const Keyv = require('keyv')
  const CacheDatastore = require('keyv-nedb-core')

  const { logSilly, logWarning, logError } = T.setupLogging()

  const prefix = 'STORAGE |'
  const ttlField = state.get('configuration', 'ttlField')
  const keyField = state.get('configuration', 'keyField')

  state.set('prefix', prefix)
  let stores = {}

  function setup (job) {
    if (!R.isNil(stores[job])) {
      return stores[job]
    }
    let basedir = state.get('configuration', 'storageDirectory')
    stores[job] = new Keyv({
      store: new CacheDatastore({
        filename: pathlib.resolve(basedir, job + '.memory'),
        autoload: true
      })
    })
    stores[job].on('error', R.curry(logError)(state, 'Received an error from the underlying storage library', R.__, null))
    return stores[job]
  }

  async function record (job, fact) {
    let key = fact.apply(R.prop(keyField))
    let ttl = fact.apply(R.prop(ttlField))
    if (!R.isNil(key)) {
      await stores[job].set(key, fact, ttl * 1000)
      logSilly(state, 'A fact was recorded for job ' + job + ', key ' + key)
    } else {
      logWarning(state, 'A fact learned by job ' + job + ' was discarded because it had no key.')
    }
    return fact
  }

  async function erase (job, fact) {
    let key = fact.apply(R.prop(keyField))
    await stores[job].delete(key)
    logWarning(state, 'A fact learned by job ' + job + ' was erased (key: ' + key + ')')
    return fact
  }

  async function exists (job, key) {
    let data = await stores[job].get(key)
    return !R.isNil(data)
  }

  async function shrink (job) {
    await stores[job].opts.store.evictExpired()
    logSilly(state, 'Done shrinking the memory of job ' + job)
    return true
  }

  return {
    setup,
    record: R.curry(record),
    erase: R.curry(erase),
    exists: R.curry(exists),
    shrink
  }
}
