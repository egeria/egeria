# [![Egeria](https://gitlab.com/egeria/egeria-temple/raw/master/logo/logo_small.png)](https://gitlab.com/egeria/egeria-temple) Temple

[![NPM version](http://img.shields.io/npm/v/egeria-temple.svg)](https://www.npmjs.com/package/egeria-temple)
[![Downloads](https://img.shields.io/npm/dm/egeria-temple.svg)](https://www.npmjs.com/package/egeria-temple)

![Egeria's temple](http://i.imgur.com/E52uuFX.jpg)<br />
*The temple of Egeria, the SFW nymph, in Rome*

[![NPM](https://nodei.co/npm/egeria-temple.png?downloads=true&stars=true)](https://nodei.co/npm/egeria-temple/)

## Changelog
v0.7.1
 - Add `cleanTemplate` to remove tokens from a template and get the static part

v0.7.0
 - Export functions `getFields`, `getValues`, `getEverything`

v0.6.0
 - `defaults` now returns a wrapper, kinda like request does, so multiple defaults can be set across a project.

v0.5.0
 - Breaking changes
 - Exports two functions: `temple` is the old `template` function; `defaults` lets user change the default date format and provide a hook that temple will call when errors occur.
 - `temple` only returns the parsed text; no errors


## About

Dead simple templating functionality for [Egeria](https://github.com/MySidesTheyAreGone/egeria).

The module exports two functions.

The `defaults` function accepts an object with any of the following fields:
 - `dateFormat` sets the default format whenever temple renders a date token. The default is 'YYYYMMDD'. You can change it to anything supported by `moment`.
 - `onError` must be a function. When rendering a template, if any of the tokens are malformed then `onError` will be called with a string as its only argument. The string will contain a list of **all** of the errors. Every time `temple` is called, `onError` will be called either once or never.

`defaults` returns a wrapper; use that wrapper instead of `temple`.

The `temple` function expects the following arguments: `template` and `data`, in this order. `template` is always a string and `data` is either an object with one or more fields or a single value - Number, String, or anything else. If either is null, the output text will be ''.

A template is a single string that contains zero or more tokens. Text present between tokens won't be changed.

In a template, "||" and "!!" are reserved sequences. "|||", "||||" and so on are all illegal. The same goes for "!!" and "!!!", "!!!!" and so on. It's OK if those sequences *are part of the value that will be rendered*, but they must not appear in the template itself.

A token is written like this: `{{PRE||FIELD!!FORMAT||POST}}`

The order of the various elements must be respected:

`PRE` is a string that will precede the value.

`FIELD` is a field of the second argument, `data`. If data is not an object, there will be only one valid field called "arg", so your tokens can only reference "arg" (e.g. 'Joe had {{arg|| children and}} 1 mule').

`FORMAT` is the format that will be used to format the `FIELD` if and only if such value is a Date. It will be ignored otherwise.

`POST` is a string that will be appended to the value.

`PRE`, `FORMAT` and `POST` are optional. If the value is a Date, the default `FORMAT` is 'YYYYMMDD' (e.g. "20151124").

`FIELD` is mandatory, but it doesn't have to exist on `data`. If it's null, undefined or empty, the entire token will be replaced by an empty string, meaning that `PRE` and `POST` will not be rendered either.

Malformed tokens will remain untouched in the final output.

The smalles token you can possibly write is `{{fieldName}}`. The biggest is `{{On day ||happeningDate!!DD-MM-YYYY|| something happened}}`.

The function returns a string containing the result.

## Example

```js
var temple = require('egeria-temple').defaults({
    defaultDateFormat: 'DD/MM/YYYY',
    onError: console.log
});

var template = '{{title|| - }}{{on ||date|| - }}{{text}}{{fsdjhgsj^#||wrongField|| you won\'t see this}}';

var data = {
	title: 'one',
	date: new Date(),
	text: 'some long text'
}

console.log(temple(template, data).text);

//> "one - on 25/11/2015 - some long text"


template = '{{title|| - }}{{date!!MM/YYYY-DD|| - }}{{text}}';

console.log(temple(template, data).text);

//> "one - 11/2015-25 - some long text"

```
