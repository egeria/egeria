/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const moment = require('moment')

var defaults = {
  dateFormat: 'YYYYMMDD',
  onError: R.identity,
  outFormat: 'string'
}

var removeTokens = R.replace(/{{[^{}]*}}/g, '')
var tokenize = R.match(/{{[^}]*}}/g)
var clean = R.replace(/[{}]+/g, '')
var separate = R.match(/[^||]+/g)
var format = (e) => {
  if (R.test(/!!/, e)) return R.match(/[^!!]+/g, e)
  return e
}
var deconstruct = R.pipe(tokenize, R.map(clean), R.map(separate), R.map(R.map(format)))
var analyze = R.converge(R.zip, [tokenize, deconstruct])

var render = (defs, template, arg) => {
  if (R.isNil(template)) template = ''

  if (defs.outFormat === 'clean') return removeTokens(template)

  var data = R.isNil(arg) ? {} : arg
  if (!R.is(Object, data)) data = { arg: arg }

  var out = template
  var fields = []
  var values = []
  var errors = []
  var tokens = analyze(template)

  for (var i = 0, l = tokens.length; i < l; ++i) {
    var token = tokens[i][0]
    var elements = tokens[i][1]
    var pre, value, format, post, field, maybeField
    if (elements.length > 4 || elements.length < 1) { // malformed, interrupt and do nothing
      errors.push('malformed token: "' + token + '"')
      continue
    } else {
      maybeField = elements.shift()
      if (R.is(Array, maybeField)) {
        field = maybeField
        pre = ''
      } else if (!data.hasOwnProperty(maybeField)) {
        field = elements.shift()
        pre = maybeField
      } else {
        field = maybeField
        pre = ''
      }
      post = R.defaultTo('', elements.shift())

      if (elements.length > 0) {
        errors.push('malformed token: "' + token + '"')
        continue
      }

      if (R.is(Array, field)) {
        if (field.length > 2) {
          errors.push('malformed token: "' + token + '"')
          continue
        }
        format = R.defaultTo(defs.dateFormat, field[1])
        field = field[0]
      } else {
        format = defs.dateFormat
      }

      value = data[field]

      fields.push(field)
      values.push(value)

      if (R.isNil(value)) {
        out = R.replace(token, '', out)
      } else if (R.is(Date, value)) {
        out = R.replace(token, pre + moment(value).format(format) + post, out)
      } else {
        out = R.replace(token, pre + data[field] + post, out)
      }
    }
  }

  if (errors.length !== 0) {
    errors.unshift('In template "' + template + '"')
    defs.onError(R.join('; ', errors))
  }

  if (defs.outFormat === 'string') return out
  if (defs.outFormat === 'values') return values
  if (defs.outFormat === 'fields') return fields
  if (defs.outFormat === 'complete') return { string: out, values: values, fields: fields }
}

var setDefs = (defs) => {
  var newDefaults = R.merge(defaults, defs)
  return R.curry(render)(newDefaults)
}

module.exports = {
  temple: R.curry(render)(defaults),
  cleanTemplate: R.curry(render)(R.assoc('outFormat', 'clean', defaults), R.__, {}),
  getValues: R.curry(render)(R.assoc('outFormat', 'values', defaults)),
  getFields: R.curry(render)(R.assoc('outFormat', 'fields', defaults)),
  getEverything: R.curry(render)(R.assoc('outFormat', 'complete', defaults)),
  defaults: setDefs
}
