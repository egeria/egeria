/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const temple = require('@egeria/temple').temple

var Knowledge = function (x) {
  let y

  if (R.isNil(x)) {
    this._value = null
  } else {
    if (R.isNil(x._dates)) {
      let dates = []
      y = R.mapObjIndexed((value, key) => {
        if (R.is(Date, value)) {
          dates.push(key)
        }
        return value
      }, x)
      y._dates = dates
      this._value = y
    } else {
      y = R.mapObjIndexed((value, key) => {
        if (key === '_dates') return value
        if (R.contains(key, x._dates)) {
          return new Date(value)
        } else {
          return value
        }
      }, x)
      this._value = y
    }
  }
}

Knowledge.of = function (x) {
  if (R.is(Knowledge, x)) {
    return x
  } else if (R.is(String, x)) {
    return new Knowledge(JSON.parse(x))
  } else {
    return new Knowledge(x)
  }
}

Knowledge.prototype = {
  isNil () {
    return R.isNil(this._value)
  },
  map (fn) {
    if (this.isNil()) {
      return Knowledge.of(null)
    } else {
      let clone = R.clone(R.dissoc('_dates', this._value))
      let result = fn(clone)
      return Knowledge.of(result)
    }
  },
  apply (fn) {
    if (this.isNil()) {
      return null
    } else {
      let clone = R.clone(this._value)
      let result = fn(clone)
      return result
    }
  },
  getTags () {
    return R.defaultTo([], this.apply(R.prop('system:tags')))
  },
  isTagged (tags) {
    let appliedTags = this.getTags()
    if (appliedTags.length === 0 || R.defaultTo([], tags).length === 0) {
      return true
    } else {
      let tagged = R.map(R.contains(R.__, appliedTags), tags)
      return R.reduce(R.or, false, tagged)
    }
  },
  applyTemplate (tmpl) {
    return this.apply(temple(tmpl))
  },
  toJSON () {
    return this.isNil() ? null : JSON.stringify(this._value)
  },
  empty () {
    return Knowledge.of(null)
  }
}

module.exports = Knowledge
