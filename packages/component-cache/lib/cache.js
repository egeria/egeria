/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

module.exports = function () {
  const R = require('ramda')
  const Keyv = require('keyv')

  let cache = new Keyv()
  let setops = 0

  async function remember (opts) {
    let data = await cache.get(opts.key)
    if (!R.isNil(data)) {
      return data
    }
    if (R.isNil(opts.fn)) {
      return null
    }
    data = await opts.fn()
    let ttl = opts.ttl
    if (ttl === 0 || ttl === Infinity) {
      ttl = null
    } else {
      ttl = ttl * 1000
    }
    await cache.set(opts.key, data, ttl)
    if (++setops >= 250) {
      setops = 0
      // await cache.shrink()
    }
    return data
  }

  async function invalidate (key) {
    return cache.delete(key)
  }

  async function set (key, data, ttl) {
    await cache.set(key, data, ttl)
    return data
  }

  function get (key) {
    return cache.get(key)
  }

  return {
    run: remember,
    set,
    get,
    invalidate
  }
}
