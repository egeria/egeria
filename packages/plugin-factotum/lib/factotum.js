/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const froid = R.curry(R.binary(require('froid')))

const buildPropertySetter = R.pipe(
  R.map(R.converge(R.curry((a, b, f) => R.map(R.assoc(a, f.applyTemplate(b)), f)), [
    R.nth(0),
    R.nth(1)
  ])),
  R.values,
  R.apply(R.pipe)
)

async function adjustAct (api, cfg = {}, fact) {
  let set = buildPropertySetter(cfg.set)
  let result = set(fact)
  return result
}

async function dedupeAct (api, cfg = {}, fact) {
  let keyTemplate = R.defaultTo('{{key}}', cfg.key)
  let key = fact.applyTemplate(keyTemplate)
  let keyExists = await api.storage.exists(key)
  if (keyExists) {
    return fact.empty()
  } else {
    await api.storage.record(fact)
    return fact
  }
}

async function taggerAct (api, cfg = {}, fact) {
  let schemas = cfg.schemas
  let schemaTags = cfg.schemaTags
  let newTags = []
  for (let i = 0, l = schemas.length; i < l; ++i) {
    let schema = schemas[i]
    let err = fact.apply(froid(schema))
    if (R.isNil(err)) {
      newTags.push(schemaTags[i])
    }
  }
  if (R.isEmpty(newTags)) {
    return fact
  } else {
    return fact.map(R.over(R.lensProp('system:tags'), R.pipe(R.defaultTo([]), R.concat(newTags))))
  }
}

let replace = R.curry((def, fact) => {
  let value = fact.applyTemplate(def.val)
  let exp = fact.applyTemplate(def.exp)
  let mod = fact.applyTemplate(def.mod)
  let sub = fact.applyTemplate(def.sub)
  let field = fact.applyTemplate(def.field)
  let reg

  if (R.isEmpty(mod) || R.isNil(mod)) {
    reg = new RegExp(exp)
  } else {
    reg = new RegExp(exp, mod)
  }

  let newValue = value.replace(reg, sub)
  return R.map(R.assoc(field, newValue), fact)
})

async function regexpAct (api, cfg = {}, fact) {
  let replacements = cfg.replacements
  return R.apply(R.pipe, R.map(replace, replacements))(fact)
}

async function filterAct (api, cfg = {}, fact) {
  var err = fact.apply(froid(cfg))
  if (R.isNil(err)) {
    return fact
  } else {
    return fact.empty()
  }
}

let plugins = {}

plugins.adjust = {
  type: 'mutator',
  sanity: {
    type: 'object',
    required: ['set'],
    properties: { set: { type: 'array' } }
  },
  act: adjustAct
}

plugins.deduplicate = {
  type: 'mutator',
  requires: ['storage'],
  limits: {
    upstream: 'storage',
    concurrency: 1,
    delay: 0.1,
    timeout: 60
  },
  sanity: { type: 'object', properties: { key: { type: 'string' } } },
  act: dedupeAct
}

plugins.tagger = {
  type: 'mutator',
  sanity: {
    type: 'object',
    required: ['schemas', 'schemaTags'],
    properties: {
      schemas: { type: 'array' },
      schemaTags: { type: 'array' }
    }
  },
  act: taggerAct
}

plugins.regexp = {
  type: 'mutator',
  sanity: {
    type: 'object',
    required: ['replacements'],
    properties: {
      replacements: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            field: { type: 'string' },
            val: { type: 'string' },
            exp: { type: 'string' },
            mod: { type: 'string' },
            sub: { type: 'string' }
          }
        }
      }
    }
  },
  act: regexpAct
}

plugins.filter = {
  type: 'mutator',
  sanity: { type: 'object' },
  act: filterAct
}

module.exports = plugins
