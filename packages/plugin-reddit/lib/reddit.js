/*
.--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

  Egeria - She bestows Knowledge and Wisdom
  Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

.--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const Snoo = require('snoowrap')

const userAgent = 'Node.js/' + process.version + ' Egeria/reddit-plugin v' + require('../package.json').version

async function getClient (identity) {
  let snoo = new Snoo({
    user_agent: userAgent,
    client_id: identity.clientId,
    client_secret: identity.clientSecret,
    username: identity.username,
    password: identity.password
  })
  snoo.config({
    requestDelay: 0,
    requestTimeout: 120000,
    retryErrorCodes: [],
    maxRetryAttempts: 0,
    warnings: false,
    proxies: false
  })
  let client = {
    identity,
    snoo
  }
  return client
}

async function getIdentity (client) {
  return client.identity
}

const cleanListingItem = R.pipe(
  R.dissocPath(['author', '_r']),
  R.dissocPath(['author', '_fetch']),
  R.dissocPath(['author', '_hasFetched']),
  R.omit(['_r', '_fetch', '_hasFetched']),
  R.dissoc('comments')
)

function processListingItem (item) {
  let processedItem = cleanListingItem(item)
  processedItem.created = new Date(processedItem.created * 1000)
  processedItem.created_utc = new Date(processedItem.created_utc * 1000)
  if (!R.isNil(processedItem.context)) {
    processedItem.context = 'http://www.reddit.com' + processedItem.context
  }
  if (!R.isNil(processedItem.subreddit)) {
    processedItem.subreddit = processedItem.subreddit.display_name
  }
  processedItem.key = 'reddit:' + processedItem.name
  processedItem = T.collapse('reddit:', processedItem)
  processedItem.key = processedItem['reddit:key']
  processedItem.origin = 'reddit'
  return processedItem
}

async function getPosts (api, cfg, subreddit) {
  let { enqueue, announce } = api
  let method = (cfg.mode === 'new') ? 'get_new' : 'get_hot'
  let opts = { limit: cfg.limit }
  let posts = await enqueue(() => api.client.snoo.get_subreddit(subreddit)[method](opts))
  for (let i = 0, l = posts.length; i < l; ++i) {
    let post = posts[i]
    post = await enqueue(() => post.fetch())
    post = processListingItem(post)
    post['reddit:permalink'] = 'http://www.reddit.com' + post['reddit:permalink']
    announce(post)
  }
  return true
}

async function getPrivateMessages (api, cfg) {
  let { enqueue, announce } = api
  let messages = await enqueue(() => api.client.snoo.getUnreadMessages())
  for (let i = 0, l = messages.length; i < l; ++i) {
    let message = messages[i]
    if (cfg.markAsRead) {
      await enqueue(() => api.client.snoo.markMessagesAsRead([message]))
    }
    if (!R.isNil(message.permalink)) {
      message.permalink = 'https://www.reddit.com/message/messages/' + message.id
    } else {
      message.permalink = 'http://www.reddit.com' + message.permalink
    }
    message = R.dissoc('replies', message)
    message = processListingItem(message)
    announce(message)
  }
}

async function redditAct (api, cfg) {
  let request
  if (cfg.mode === 'messages') {
    let action = 'While fetching private messages for ' + api.client.identity.name
    try {
      request = await getPrivateMessages(api, cfg)
    } catch (e) {
      T.err(action, e)
    }
  } else {
    let subreddits = R.flatten(R.of(cfg.subreddit))
    let action = 'While fetching posts from ' + R.join(', ', subreddits)
    try {
      let requests = []
      for (let subreddit of subreddits) {
        requests.push(getPosts(api, cfg, subreddit))
      }
      request = await Promise.all(requests)
    } catch (e) {
      T.err(action, e)
    }
  }
  return request
}

async function redditSubmitAct (api, cfg, fact) {
  let { enqueue } = api
  let action = 'While submitting a post'
  let subredditName = fact.applyTemplate(cfg.subreddit)
  let title = fact.applyTemplate(cfg.title)
  if (cfg.mode === 'link') {
    let url = fact.applyTemplate(cfg.link)
    try {
      await enqueue(() => api.client.snoo.submit_link({ subredditName, title, url }))
    } catch (e) {
      T.err(action, e)
    }
  } else {
    let text = fact.applyTemplate(cfg.text)
    try {
      await enqueue(() => api.client.snoo.submit_selfpost({ subredditName, title, text }))
    } catch (e) {
      T.err(action, e)
    }
  }
  return fact
}

const plugins = {
  reddit: {
    type: 'input',
    requires: ['identity', 'schedule'],
    limits: {
      upstream: 'reddit',
      concurrency: 1,
      delay: 1,
      timeout: 60
    },
    sanity: {
      type: 'object',
      required: ['mode', 'identity'],
      properties: {
        mode: {
          type: 'string',
          enum: ['new', 'hot', 'messages']
        },
        subreddit: { type: ['string', 'array'] },
        limit: {
          type: 'number',
          exclusiveMinimum: 0
        },
        markAsRead: { type: 'boolean' }
      }
    },
    getClient,
    getIdentity,
    act: redditAct
  },
  redditSubmit: {
    type: 'output',
    requires: ['identity'],
    limits: {
      upstream: 'reddit',
      concurrency: 1,
      delay: 1,
      timeout: 60
    },
    sanity: {
      type: 'object',
      required: ['mode', 'subreddit', 'title', 'identity'],
      properties: {
        mode: { enum: ['link', 'self'] },
        subreddit: { type: 'string' },
        title: { type: 'string' },
        link: { type: 'string' },
        body: { type: 'string' }
      }
    },
    getClient,
    getIdentity,
    act: redditSubmitAct
  }
}

module.exports = plugins
