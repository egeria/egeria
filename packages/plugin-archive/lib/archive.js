/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const W = require('@egeria/httplib')

const twelveHours = 12 * 3600

const rethrow = R.curry((state, action, e) => {
  throw T.err(state.get('prefix'), action, e)
})

const clean = R.pipe(R.defaultTo(''), R.replace(/\r?\n|\r/g, ' '), R.trim, R.replace(/\s+/g, ' '))

const scrapeSubmitID = R.pipe(
  R.prop('body'),
  W.scrapeHTML('input[name=submitid]', 'value')
)

function extractArchiveURL (res) {
  let url
  if (res.request.href === 'http://archive.is/submit/') {
    url = R.replace(/.*document\.location\.replace\("(.*)"\).*/, '$1', clean(res.body))
  } else {
    url = res.request.href
  }
  return url
}

async function act (api, cfg, fact) {
  let url = fact.applyTemplate(cfg.url)
  let field = R.defaultTo('archive:url', fact.applyTemplate(cfg.field))
  let action = 'While opening archive.is\' homepage'
  let cachedArchive = await api.cache.get(url)
  if (!T.isMissing(cachedArchive)) {
    return cachedArchive
  }
  let archiveRequestPage
  try {
    archiveRequestPage = await api.enqueue(() => W.cfhttpreq({ uri: 'http://archive.is' }))
  } catch (e) {
    rethrow(action, e)
  }
  let submitid = scrapeSubmitID(archiveRequestPage.body)
  action = 'While submitting "' + url + '" to archive.is'
  let archivedPage
  try {
    archivedPage = await api.enqueue(() => W.cfhttpreq({
      method: 'POST',
      uri: 'http://archive.is/submit/',
      form: {
        url: url,
        submitid: submitid
      }
    }))
  } catch (e) {
    rethrow(action, e)
  }
  let archive = extractArchiveURL(archivedPage)
  await api.cache.set(url, archive, twelveHours)
  return fact.map(R.assoc(field, archive))
}

const plugins = {
  archiveIs: {
    type: 'mutator',
    requires: ['cache'],
    sanity: {
      type: 'object',
      required: ['url'],
      properties: {
        url: { type: 'string' },
        field: { type: 'string' }
      }
    },
    limits: {
      upstream: 'archiveis',
      concurrency: 1,
      delay: 1,
      timeout: 60
    },
    act
  }
}

module.exports = plugins
