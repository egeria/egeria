# [![Egeria](https://gitlab.com/egeria/egeria-divine/raw/master/logo/logo_small.png)](https://gitlab.com/egeria/egeria-divine) Divine

[![NPM version](http://img.shields.io/npm/v/egeria-divine.svg)](https://www.npmjs.com/package/egeria-divine)
[![Downloads](https://img.shields.io/npm/dm/egeria-divine.svg)](https://www.npmjs.com/package/egeria-divine)


## Warning

This thing still needs a lot of testing.

## Usage

`divine` is a single function that extracts information from the name of a video file.

The regular expressions used by `divine` are [from](https://github.com/Flexget/Flexget/blob/develop/flexget/utils/qualities.py) the excellent [Flexget](https://github.com/Flexget/Flexget) project.

`divine` returns an object like this:

```js
{
    videoResolution: '1080p',
    videoSource: 'bluray',
    videoCodec: 'h264',
    audioCodec: 'aac'
}
```

Should two different qualities be indicated by the same filename (very unlikely), the best one will be returned. This should be made configurable in a later update.

Things to note:
 - DTS and DTS-HD are both indicated as `dts`
 - AVC is correctly interpreted as `h264`. There seems to be no difference between the two formats.
 - You may get `null` in one or more of the fields if the filename doesn't indicate anything `divine` can recognize. This is not uncommon.


## Supported qualities

I knew I'd have to type all this :(

Resolutions: "360p", "368p", "480p", "576p", "720i", "720p", "1080i", "1080p"

Video codecs: "divx", "xvid", "h264", "h265", "10bit"

Audio codecs: "mp3", "aac", "dd5.1", "ac3", "flac", "dts", "dtshd", "truehd"

Totally legal sources: "workprint", "cam", "ts", "tc", "r5", "hdrip", "ppvrip", "preair", "tvrip", "dsr", "sdtv", "webrip", "dvdscr", "bdscr", "hdtv", "webdl", "dvdrip", "remux", "bluray",

## Example

Here's an example using filenames taken from totally legal torrents:

```js
var R = require('ramda'),
    divine = require('egeria-divine');

var tests = [
    'Totally.Legal.2015.1080p.BluRay.AVC.DTS-HD.MA.5.1-RARBG',
    'Totally.Legal.2015.1080p.BluRay.AVC.DTS-HD.MA.5.1-RARBG',
    '12.Years.Totally.Legal.2013.720p.BluRay.H264.AAC-RARBG',
    'Totally.Legal.2015.1080p.BluRay.REMUX.AVC.DTS-HD.MA.5.1-RARBG',
    'Totally.Legal.in.Their.Eyes.2015.1080p.BluRay.REMUX.AVC.DTS-HD.MA.5.1-RARBG',
    'Totally.Legal.UK.S05E05.1080p.HDTV.x264-TLA[rartv]',
    'Another.Totally.Legal.Story.2008.720p.BluRay.H264.AAC-RARBG',
    'Totally.Legal.2015.1080p.BluRay.x264.DTS-HD.MA.5.1-RARBG',
    'Totally.Legal.in.Their.Eyes.2015.1080p.BluRay.x264.DTS-HD.MA.5.1-RARBG',
    'Totally.Legal.Fire.2015.1080p.BluRay.x264-VALUE',
    'Totally.Legal.Med.S01E09.WEB-DL.x264-RARBG',
    'Chicago.Totally.Legal.S04E14.WEB-DL.x264-RARBG',
    'Totally.Legal.Rebels.S02E14.The.Call.1080p.DSNY.WEBRip.AAC2.0.x264-TVSmash[rartv]',
    'Totally.Legal.Med.S01E09.Choices.1080p.WEB-DL.DD5.1.H264-NTb[rartv]',
    'Totally.Legal.Med.S01E09.Choices.720p.WEB-DL.DD5.1.H264-NTb[rartv]',
    'Chicago.Totally.Legal.S04E14.All.Hard.Parts.1080p.WEB-DL.DD5.1.H264-NTb[rartv]',
    'Chicago.Totally.Legal.S04E14.All.Hard.Parts.720p.WEB-DL.AAC2.0.H264-NTb[rartv]',
    'Totally.Legal.Fire.2015.720p.BluRay.x264-RUSTED',
    'Totally.Legal.2000.1080p.BluRay.H264.AAC-RARBG',
    'Totally.Legal.Fire.2015.BDRip.x264-RUSTED',
    'Totally.Legal.2000.720p.BluRay.H264.AAC-RARBG',
    'Totally.Legal.Evil.2012.720p.BluRay.H264.AAC-RARBG',
    'Totally.Legal.to.Wear.1994.720p.BluRay.H264.AAC-RARBG',
    'Totally.Legal.Scaredycat.2010.720p.BluRay.H264.AAC-RARBG',
    'Totally.Legal.2012.1080p.BluRay.H264.AAC-RARBG '
];

R.forEach((test) => {
    console.log(test);
    console.log(divine(test));
    console.log('----')
}, tests);

```

It will produce the following output:
```
Totally.Legal.2015.1080p.BluRay.AVC.DTS-HD.MA.5.1-RARBG
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'dts' }
----
Totally.Legal.2015.1080p.BluRay.AVC.DTS-HD.MA.5.1-RARBG
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'dts' }
----
12.Years.Totally.Legal.2013.720p.BluRay.H264.AAC-RARBG
{ videoResolution: '720p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.2015.1080p.BluRay.REMUX.AVC.DTS-HD.MA.5.1-RARBG
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'dts' }
----
Totally.Legal.in.Their.Eyes.2015.1080p.BluRay.REMUX.AVC.DTS-HD.MA.5.1-RARBG
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'dts' }
----
Totally.Legal.UK.S05E05.1080p.HDTV.x264-TLA[rartv]
{ videoResolution: '1080p',
  videoSource: 'hdtv',
  videoCodec: 'h264',
  audioCodec: null }
----
Another.Totally.Legal.Story.2008.720p.BluRay.H264.AAC-RARBG
{ videoResolution: '720p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.2015.1080p.BluRay.x264.DTS-HD.MA.5.1-RARBG
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'dts' }
----
Totally.Legal.in.Their.Eyes.2015.1080p.BluRay.x264.DTS-HD.MA.5.1-RARBG
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'dts' }
----
Totally.Legal.Fire.2015.1080p.BluRay.x264-VALUE
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: null }
----
Totally.Legal.Med.S01E09.WEB-DL.x264-RARBG
{ videoResolution: null, videoSource: 'webdl', videoCodec: 'h264', audioCodec: null }
----
Chicago.Totally.Legal.S04E14.WEB-DL.x264-RARBG
{ videoResolution: null, videoSource: 'webdl', videoCodec: 'h264', audioCodec: null }
----
Totally.Legal.Rebels.S02E14.The.Call.1080p.DSNY.WEBRip.AAC2.0.x264-TVSmash[rartv]
{ videoResolution: '1080p',
  videoSource: 'webrip',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.Med.S01E09.Choices.1080p.WEB-DL.DD5.1.H264-NTb[rartv]
{ videoResolution: '1080p',
  videoSource: 'webdl',
  videoCodec: 'h264',
  audioCodec: null }
----
Totally.Legal.Med.S01E09.Choices.720p.WEB-DL.DD5.1.H264-NTb[rartv]
{ videoResolution: '720p',
  videoSource: 'webdl',
  videoCodec: 'h264',
  audioCodec: null }
----
Chicago.Totally.Legal.S04E14.All.Hard.Parts.1080p.WEB-DL.DD5.1.H264-NTb[rartv]
{ videoResolution: '1080p',
  videoSource: 'webdl',
  videoCodec: 'h264',
  audioCodec: null }
----
Chicago.Totally.Legal.S04E14.All.Hard.Parts.720p.WEB-DL.AAC2.0.H264-NTb[rartv]
{ videoResolution: '720p',
  videoSource: 'webdl',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.Fire.2015.720p.BluRay.x264-RUSTED
{ videoResolution: '720p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: null }
----
Totally.Legal.2000.1080p.BluRay.H264.AAC-RARBG
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.Fire.2015.BDRip.x264-RUSTED
{ videoResolution: null, videoSource: 'bluray', videoCodec: 'h264', audioCodec: null }
----
Totally.Legal.2000.720p.BluRay.H264.AAC-RARBG
{ videoResolution: '720p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.Evil.2012.720p.BluRay.H264.AAC-RARBG
{ videoResolution: '720p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.to.Wear.1994.720p.BluRay.H264.AAC-RARBG
{ videoResolution: '720p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.Scaredycat.2010.720p.BluRay.H264.AAC-RARBG
{ videoResolution: '720p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
Totally.Legal.2012.1080p.BluRay.H264.AAC-RARBG
{ videoResolution: '1080p',
  videoSource: 'bluray',
  videoCodec: 'h264',
  audioCodec: 'aac' }
----
```

Have fun! Toodle-oo, go with God, and don't take any wooden nickels!
