const R = require('ramda')
const XRegExp = require('xregexp')

const isMissing = R.either(R.isNil, R.isEmpty)

const adjustments = {
  series: R.trim,
  issueNumber: parseInt,
  year: parseInt,
  extension: R.trim
}

const pattern = `
  (?<series>.*)
  \\s*
  (?<issueNumber>[0-9]{3})
  \\s*[([]*
  (?<year>[0-9]{4})
  .*
  (?<extension>\\.rar|\\.zip|\\.cbr|\\.cbz)$`

const exp = new XRegExp(pattern, 'xi')

const fallback = `
  (?<series>.*)
  \\s*
  (?<issueNumber>[0-9]{1,3})
  \\s*[([]?\\s*of\\s*[0-9]{1,3}[)\\]]?\\s*[([]?
  (?<year>[0-9]{4})
  .*
  (?<extension>\\.rar|\\.zip|\\.cbr|\\.cbz)$`

const fbexp = new XRegExp(fallback, 'xi')

// TODO: second fallback matching volume number

module.exports = function (filename) {
  let matches = XRegExp.exec(filename, exp)
  if (isMissing(matches)) {
    matches = XRegExp.exec(filename, fbexp)
  }
  if (isMissing(matches)) {
    return null
  }
  let comicbook = R.pick(['series', 'issueNumber', 'year', 'extension'], matches)
  comicbook = R.evolve(adjustments, comicbook)
  comicbook.isFirstIssue = comicbook.issueNumber === 1 || comicbook.issueNumber === 0
  comicbook.filename = filename
  return comicbook
}
