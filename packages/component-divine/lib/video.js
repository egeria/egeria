const R = require('ramda')

const videoChannelsRE = '(?:(?:[\W_]?5[\W_]?1)|(?:[\W_]?2[\W_]?(?:0|ch)))' // eslint-disable-line

const videoQuality = {
  videoResolution: [
    { name: '360p', re: '360' },
    { name: '368p', re: '368p?' },
    { name: '480p', re: '480p?' },
    { name: '576p', re: '576p?' },
    { name: '720i', re: '720i' },
    { name: '720p', re: '(1280x)?720(p|hd)?x?(50)?' },
    { name: '1080i', re: '1080i' },
    { name: '1080p', re: '(1920x)?1080p?' }
  ],
  videoSource: [
    { name: 'workprint', re: 'workprint' },
    { name: 'cam', re: '(?:hd)?cam' },
    { name: 'ts', re: '(?:hd)?ts|telesync' },
    { name: 'tc', re: 'tc|telecine' },
    { name: 'r5', re: 'r[2-8c]' },
    { name: 'hdrip', re: 'hd[\W_]?rip' }, // eslint-disable-line
    { name: 'ppvrip', re: 'ppv[\W_]?rip' }, // eslint-disable-line
    { name: 'preair', re: 'preair' },
    { name: 'tvrip', re: 'tv[\W_]?rip' }, // eslint-disable-line
    { name: 'dsr', re: 'dsr|ds[\W_]?rip' }, // eslint-disable-line
    { name: 'sdtv', re: '(?:[sp]dtv|dvb)(?:[\W_]?rip)?' }, // eslint-disable-line
    { name: 'webrip', re: 'web[\W_]?rip' }, // eslint-disable-line
    { name: 'dvdscr', re: '(?:(?:dvd|web)[\W_]?)?scr(?:eener)?' }, // eslint-disable-line
    { name: 'bdscr', re: 'bdscr(?:eener)?' },
    { name: 'hdtv', re: 'a?hdtv(?:[\W_]?rip)?' }, // eslint-disable-line
    { name: 'webdl', re: 'web(?:[\W_\-]?(dl|hd))' }, // eslint-disable-line
    { name: 'dvdrip', re: 'dvd(?:[\W_]?rip)?' }, // eslint-disable-line
    { name: 'remux', re: 'remux' },
    { name: 'bluray', re: '(?:b[dr][\W_]?rip|blu[\W_]?ray(?:[\W_]?rip)?)' } // eslint-disable-line
  ],
  videoCodec: [
    { name: 'divx', re: 'divx' },
    { name: 'xvid', re: 'xvid' },
    { name: 'h264', re: '([hx].?264|AVC)' },
    { name: 'h265', re: '[hx].?265|hevc' },
    { name: '10bit', re: '10.?bit|hi10p' }
  ],
  audioCodec: [
    { name: 'mp3', re: 'divx' },
    { name: 'aac', re: 'aac' + videoChannelsRE + '?' },
    { name: 'dd5.1', re: 'dd' + videoChannelsRE },
    { name: 'ac3', re: 'ac3' + videoChannelsRE + '?' },
    { name: 'flac', re: 'flac' + videoChannelsRE + '?' },
    { name: 'dts', re: 'dts' },
    { name: 'dtshd', re: 'dts[\W_]?hd(?:[\W_]?ma)?' }, // eslint-disable-line
    { name: 'truehd', re: 'truehd' }
  ]
}

let makeAheadRegExp = (exp) => new RegExp('.(' + exp + ')(?![\w])', 'gi') // eslint-disable-line
let behindRegExp = /^[\w]/

let testVideoVariant = R.curry((filename, re) => R.pipe(
  R.match(makeAheadRegExp(re)),
  R.ifElse(
    R.isEmpty,
    R.always(false),
    R.pipe(
      R.map(R.pipe(
        R.match(behindRegExp),
        R.isEmpty
      )),
      R.reduce(R.or, false)
    )
  )
)(filename))

let pickBestVideoVariant = (filename) => R.pipe(
  R.filter(R.pipe(R.prop('re'), testVideoVariant(filename))),
  R.last,
  R.ifElse(R.isNil, R.always(null), R.prop('name'))
)

module.exports = (filename) => R.mapObjIndexed(pickBestVideoVariant(filename), videoQuality)
