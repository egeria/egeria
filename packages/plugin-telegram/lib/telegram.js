/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const TelegramBot = require('node-telegram-bot-api')

async function getClient (identity) {
  let tg = new TelegramBot(identity.token)
  return {
    tg,
    active: false,
    identity
  }
}

async function getIdentity (client) {
  return client.identity
}

function telegramListenAction (api, cfg, msg, parts) {
  if (T.isMissing(msg)) {
    if (!api.client.active) {
      api.client.tg.startPolling()
      api.client.active = true
    }
    for (let cmdexp of cfg.commandRegexp) {
      let exp = T.mkRegExp(cmdexp)
      api.client.tg.onText(exp, R.curry(telegramListenAction)(api, cfg))
    }
    return
  }
  let metadata = { chat: msg.chat.id }
  R.addIndex(R.forEach)((parm, idx) => (metadata['parameter' + idx] = parm), parts)
  metadata = T.collapse('telegram:', metadata)
  metadata.origin = 'telegramListen'
  api.announce(metadata)
}

function genMessage (fact, lines) {
  return R.pipe(
    R.map(R.bind(fact.applyTemplate, fact)),
    R.reject(R.either(R.isEmpty, R.isNil)),
    R.join('\n')
  )(lines)
}

function telegramSendAction (api, cfg, fact) {
  let chats = R.flatten(R.of(cfg.chat))
  let lines = R.flatten(R.of(cfg.message))
  let message = genMessage(fact, lines)
  let messageOptions = { parse_mode: 'HTML' }
  let chatNames = R.map(R.bind(fact.applyTemplate, fact), chats)
  let actions = []
  for (let chat of chatNames) {
    actions.push(api.enqueue(() => api.client.tg.sendMessage(chat, message, messageOptions)))
  }
  return Promise.all(actions).then(R.always(fact))
}

let tgLimits = {
  upstream: 'telegram',
  concurrency: 1,
  delay: 2,
  timeout: 60
}

const plugins = {
  telegramListen: {
    type: 'input',
    requires: ['announce', 'identity', 'activation'],
    sanity: {
      type: 'object',
      required: ['identity', 'commandRegexp'],
      properties: { commandRegexp: { type: ['string', 'array'] } }
    },
    limits: tgLimits,
    getClient,
    getIdentity,
    act: telegramListenAction
  },
  telegramSend: {
    type: 'output',
    requires: ['identity'],
    sanity: {
      type: 'object',
      required: ['identity', 'chat', 'message'],
      properties: {
        chat: { type: ['string', 'array'] },
        message: { type: ['string', 'array'] }
      }
    },
    limits: tgLimits,
    getClient,
    getIdentity,
    act: telegramSendAction
  }
}

module.exports = plugins
