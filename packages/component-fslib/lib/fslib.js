/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

module.exports = (function () {
  const R = require('ramda')
  const fs = require('fs-extra')

  async function decideByStat (fn, path) {
    try {
      let s = await fs.stat(path)
      return s[fn]()
    } catch (e) {
      return false
    }
  }

  const isFile = R.curry(decideByStat)('isFile')
  const isDirectory = R.curry(decideByStat)('isDirectory')

  async function size (path) {
    if (isFile(path)) {
      let s = await fs.stat(path)
      return s.size
    } else {
      return 0 // consider doing something like "du" if isDirectory
    }
  }

  return {
    ...fs,
    exists: fs.pathExists,
    isFile,
    isDirectory,
    size,
    read: fs.readFile,
    save: fs.writeFile,
    erase: fs.remove,
    mkdir: fs.ensureDir
  }
})()
