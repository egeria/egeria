/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const W = require('@egeria/httplib')

const prefix = 'PLUGIN chronoggDaily |'

const endpoint = 'https://api.chrono.gg/sale'

async function act (api) {
  try {
    let sale = R.prop('body', await api.enqueue(() => W.httpreq({
      method: 'get',
      json: true,
      uri: endpoint
    })))
    let key = R.replace(/.*?=/, '', sale.unique_url)
    let metadata = T.collapse('chronogg:', {
      game: sale.name,
      url: sale.unique_url,
      steamPage: sale.steam_url,
      image: sale.og_image,
      promoImage: sale.promo_image,
      price: sale.sale_price,
      discount: sale.discount,
      normalPrice: sale.normal_price,
      key
    })
    metadata.origin = 'chronoggDaily'
    metadata.key = key
    api.announce(metadata)
  } catch (e) {
    throw T.err(prefix, 'While fetching the daily sale data', e)
  }
}

const plugins = {
  chronoggDaily: {
    type: 'input',
    requires: ['schedule'],
    sanity: {},
    limits: {
      upstream: 'httpdl',
      concurrency: 2,
      delay: 1,
      timeout: 60
    },
    act
  }
}

module.exports = plugins
