/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

module.exports = (function () {
  const R = require('ramda')

  let po
  try {
    po = require('prettyoutput')
  } catch (e) {
    po = (obj) => R.toString(obj)
  }

  function EError (prefix, message, origin, data) {
    this.name = 'Error'
    this.prefix = prefix || 'Unknown context |'
    this.message = prefix + ' ' + message // Error compat
    this._message = message
    this.data = data
    this.origin = origin
    this.isEError = true
    Error.captureStackTrace(this, EError)
  }

  require('util').inherits(EError, Error)

  EError.prototype = {
    getMessage () {
      var msg
      if (R.isNil(this._message)) {
        msg = this.prefix + ' '
      } else {
        msg = this.prefix + ' ' + this._message + '\n'
      }
      if (!R.isNil(this.origin)) {
        if (this.origin.isEError) {
          msg += this.origin.getMessage()
        } else {
          msg += this.origin.message + '\n'
        }
      }
      return msg
    },
    getStack () {
      if (R.isNil(this.origin)) {
        return this.stack
      } else if (this.origin.isEError) {
        return this.origin.getStack()
      } else if (R.is(Error, this.origin)) {
        return this.origin.stack
      } else {
        return JSON.stringify(this.origin)
      }
    },
    getDebugData () {
      let dd = R.isNil(this.data) ? '' : this.prefix + ' debug data:\n' + po(this.data)
      if (!R.isNil(this.origin) && this.origin.isEError) {
        dd += '\n' + this.origin.getDebugData()
      }
      return dd
    },
    toString () {
      var msg = this.getMessage()
      var stack = this.getStack()
      return msg + stack
    }
  }

  EError.of = R.curry((prefix, message, data, origin) => {
    return new EError(prefix, message, data, origin)
  })

  EError.stringify = (e) => {
    return e.toString()
  }

  return EError
})()
