/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

module.exports = function (dateFormat, opts) {
  const R = require('ramda')
  const T = require('@egeria/tools')

  const cfg = R.merge({
    logLevels: {
      silly: true,
      debug: true,
      info: true,
      warning: true,
      error: true
    }
  }, opts)

  function getTimestamp () {
    return T.dateFormat(dateFormat, Date.now())
  }

  async function log (data) {
    if (!cfg.logLevels[data.level]) {
      return
    }
    if (R.isNil(data.message)) {
      console.log(`${getTimestamp()} [${data.level || 'info'}]\n${JSON.stringify(data, null, 2)}`)
      return
    }
    let message
    if (data.message.isEError) {
      message = data.message.getMessage()
    } else {
      message = R.toString(data)
    }
    if (data.level === 'error') {
      message += data.message.isEError ? data.message.getStack() : data.message.stack
      message += data.message.getDebugData()
      console.error(`${getTimestamp()} [${data.level}] ${R.init(message)}`)
    } else {
      console.log(`${getTimestamp()} [${data.level}] ${R.init(message)}`)
    }
  }

  return { log }
}
