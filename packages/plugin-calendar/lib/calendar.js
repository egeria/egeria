/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const Google = require('googleapis').google
const M = require('moment')
const T = require('@egeria/tools')

const prefix = 'PLUGIN calendarAdd'

const limits = {
  upstream: 'gcalendar',
  concurrency: 4,
  delay: 1,
  timeout: 30
}

const discoveryLimits = {
  upstream: 'gcalendar:discovery',
  concurrency: 1,
  delay: 0.25,
  timeout: 30
}

const rethrow = R.curry((action, e) => {
  throw T.err(prefix, action, e)
})

function request (client, enqueue, group, method, args) {
  var action = 'While calling API method "' + group + '.' + method + '" with arguments ' + JSON.stringify(args)
  return enqueue(() => new Promise((resolve, reject) => client[group][method](args, (err, res) => {
    if (R.isNil(err)) {
      resolve(res.data)
    } else {
      reject(T.err(prefix, action, err, { group, method, args }))
    }
  })))
}

async function getClient (identity) {
  let googleOAuth2Client = new Google.auth.OAuth2(identity.clientId, identity.clientSecret, 'none')
  googleOAuth2Client.setCredentials({
    refresh_token: identity.refreshToken
  })
  let gclient = Google.calendar({ version: 'v3', auth: googleOAuth2Client })
  return {
    google: gclient,
    oauth: googleOAuth2Client,
    identity
  }
}

async function getIdentity (client) {
  return client.identity
}

async function getCalendar (api, calendarName) {
  let action = 'While discovering the id of calendar "' + calendarName + '"'
  let calendarList
  try {
    calendarList = await api.cache.run({
      key: 'calendar:discovery:list',
      ttl: 60 * 15,
      fn: () => request(api.client.google, api.queue.enqueue(discoveryLimits), 'calendarList', 'list')
    })
  } catch (e) {
    rethrow(action, e)
  }
  let calendar
  for (let maybeCalendar of calendarList.items) {
    if (R.toUpper(maybeCalendar.summary) === R.toUpper(calendarName)) {
      calendar = T.collapse('calendar:', maybeCalendar)
      break
    }
  }
  if (R.isNil(calendar)) {
    action = 'While inserting a new calendar called "' + calendarName + '"'
    let newCalendar
    try {
      api.cache.invalidate('calendar:discovery:list')
      newCalendar = await request(api.client.google, api.queue.enqueue(discoveryLimits), 'calendars', 'insert', {
        resource: {
          summary: calendarName,
          description: calendarName
        }
      })
    } catch (e) {
      rethrow(action, e)
    }
    calendar = T.collapse('calendar:', newCalendar)
  }
  return calendar
}

async function addEvent (api, cfg, calendarId, fact) {
  let action = 'While adding an event to a calendar'
  let start = fact.applyTemplate(cfg.start)
  if (T.isMissing(start)) {
    throw T.err(prefix, action, new Error(`Could not produce a usable start date from this fact using the given property name "${cfg.start}", which evaluated to "${start}"`))
  }
  let duration = cfg.duration
  let end = cfg.end
  if (T.isMissing(end)) {
    end = M(start).add(duration[0], duration[1]).toDate()
  }
  return request(api.client.google, api.queue.enqueue(limits), 'events', 'insert', {
    calendarId,
    resource: {
      start: { dateTime: M(start).toDate() },
      end: { dateTime: end },
      description: fact.applyTemplate(cfg.description),
      summary: fact.applyTemplate(cfg.summary),
      location: fact.applyTemplate(cfg.location)
    }
  })
}

async function calendarAddAct (api, cfg, fact) {
  let calendarNameTemplates = R.flatten(R.of(cfg.calendar))
  let calendarNames = R.reject(T.isMissing, R.map((t) => fact.applyTemplate(t), calendarNameTemplates))
  for (let calendarName of calendarNames) {
    let calendar = await getCalendar(api, calendarName)
    await addEvent(api, cfg, calendar['calendar:id'], fact)
  }
  return fact
}

const plugins = {
  calendarAdd: {
    type: 'output',
    requires: ['identity', 'cache', 'multiqueue'],
    sanity: {
      type: 'object',
      required: ['identity', 'calendar'],
      properties: {
        calendar: { type: ['string', 'array'] },
        summary: { type: 'string' },
        description: { type: 'string' },
        location: { type: 'string' },
        start: { type: 'string' },
        end: { type: 'string' },
        duration: { type: 'array', maxItems: 2, minItems: 2 }
      }
    },
    act: calendarAddAct,
    getClient,
    getIdentity
  }
}

module.exports = plugins
