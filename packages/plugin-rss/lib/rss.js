/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const Feedparser = require('feedparser')
const T = require('@egeria/tools')
const _request = require('request')

const prefix = 'PLUGIN rss |'

const rethrow = R.curry((action, e) => {
  throw T.err(prefix, action, e)
})

const request = _request.defaults({
  jar: true,
  gzip: true,
  headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0' },
  /* followRedirect: function(response) {
    var T = require('egeria-tools');
    var url = require('url');
    var from = response.request.href;
    var to = url.resolve(response.headers.location, response.request.href);
    T.trace('redirected FROM ' + from);
    T.trace('redirected TO ' + response.headers.location);
    T.trace('request HEADERS ' + R.toString(response.request.headers));
    T.trace('response HEADERS ' + R.toString(response.headers));
    return true;
  }, */
  followAllRedirects: true
})

function _parseRSS (url) {
  return new Promise((resolve, reject) => {
    let items = []
    let req = request(url)
    let fp = new Feedparser()

    req.on('error', reject)
    req.on('response', function (res) {
      var stream = this
      if (res.statusCode !== 200) {
        this.emit('error', new Error('Bad status code: ' + res.statusCode))
      } else {
        stream.pipe(fp)
      }
    })

    fp.on('error', reject)
    fp.on('readable', function () {
      let item = this.read()
      while (item) {
        items.push(item)
        item = this.read()
      }
    })
    fp.on('end', () => resolve(items))
  })
}

function parseRSSItem (item) {
  let out = R.mapObjIndexed((v) => {
    if (!R.isNil(v) && !R.isNil(v['#'])) return v['#']
    if (!R.isNil(v) && !R.isNil(v['@'])) return v['@']
    return v
  }, item)
  out.key = out.link
  out.categories = R.join(', ', out.categories)
  if (!R.isNil(out.pubdate)) {
    out.pubdate = new Date(out.pubdate)
  }
  let fields = R.filter(R.test(/^rss:/), R.keys(out))
  R.forEach((k) => {
    out[R.drop(4, k)] = out[k]
    out = R.dissoc(k, out)
  }, fields)
  out = T.collapse('rss:', out)
  out.key = out['rss:key']
  return out
}

async function parseRSS (enqueue, url) {
  let action = 'While downloading and parsing RSS feed ' + url
  let items
  try {
    let rawitems = await _parseRSS(url)
    items = R.map(parseRSSItem, rawitems)
  } catch (e) {
    rethrow(action, e)
  }
  return items
}

async function act (api, cfg) {
  const urls = R.flatten(R.of(cfg.url))
  let jobs = []
  for (let url of urls) {
    jobs.push(parseRSS(api.enqueue, url).then(R.map(R.pipe(R.assoc('origin', 'rss'), api.announce))))
  }
  await Promise.all(jobs)
  return true
}

const plugins = {
  rss: {
    type: 'input',
    requires: ['schedule'],
    sanity: {
      type: 'object',
      required: ['url'],
      properties: { url: { type: ['string', 'array'] } }
    },
    limits: {
      upstream: 'http',
      concurrency: 4,
      delay: 1,
      timeout: 60
    },
    act
  }
}

module.exports = plugins
