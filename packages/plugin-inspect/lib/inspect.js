/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const pretty = require('prettyoutput')

function render (x) {
  let y = {}
  for (let prop in x) {
    if (typeof x[prop] === 'string' || typeof x[prop] === 'number' || typeof x[prop] === 'boolean') {
      y[prop] = x[prop]
    }
  }
  return pretty(y)
}

async function act (api, cfg, fact) {
  console.log('\n' + fact.apply(render))
  return fact
}

const plugins = {
  inspect: {
    type: 'mutator',
    sanity: {},
    limits: {
      upstream: 'console',
      concurrency: 1,
      delay: 0,
      timeout: 0
    },
    act
  }
}

module.exports = plugins
