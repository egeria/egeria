/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

module.exports = function (state) {
  const R = require('ramda')
  const T = require('@egeria/tools')
  const shortid = require('shortid')

  const prefix = 'QUEUE |'
  state.set('prefix', prefix)

  const err = (message, origin, data) => T.err(prefix, message, origin, data)

  let scopes = state.select('scopes')
  scopes.set({})

  let internalQueue = {}
  let timeouts = {}
  let queued = state.select('queued')
  let running = state.select('running')

  queued.set({})
  running.set({})

  function interval (delay) {
    return new Promise((resolve) => {
      if (delay === 0) {
        resolve()
      } else {
        setTimeout(resolve, delay * 1000)
      }
    })
  }

  async function fork (id, fn, res, rej, delay, state) {
    state.pending = true
    let failed = false
    let data
    try {
      await Promise.all([
        Promise.resolve(fn()).then((d) => (data = d)),
        interval(delay)
      ])
    } catch (e) {
      rej(e)
      failed = true
    }
    if (!failed) {
      res(data)
    }
    state.pending = false
    purge(id)
  }

  function enqueue (limits, fn) {
    if (T.isMissing(limits.upstream)) {
      limits.upstream = shortid.generate()
    }
    if (!scopes.select(limits.upstream).exists()) {
      scopes.select(limits.upstream).set(limits)
    }
    let id = shortid.generate()
    let consumerResolve
    let consumerReject
    let consumerPromise = new Promise((resolve, reject) => {
      consumerResolve = resolve
      consumerReject = reject
    })
    internalQueue[id] = { fn, scope: limits.upstream, consumerResolve, consumerReject }
    let job = { scope: limits.upstream, id }
    let queue = queued.select(limits.upstream)
    if (!queue.exists()) {
      queue.set([])
    }
    queue.push(job)
    process.nextTick(() => advance(limits.upstream))
    return consumerPromise
  }

  function advance (scope) {
    if (R.isNil(scope)) {
      let scopenames = R.keys(queued.get())
      return R.map(advance, scopenames)
    } else {
      let queuedInScope = queued.select(scope)
      if (!queuedInScope.exists()) {
        return []
      }
      let runningInScope = running.select(scope)
      if (!runningInScope.exists()) {
        runningInScope.set([])
      }
      let scopeCfg = scopes.select(scope).get()
      if (R.isNil(scopeCfg)) {
        throw err('Scope "' + scope + '" doesn\'t exist')
      }
      let toTake = R.max(0, scopeCfg.concurrency - runningInScope.get().length)
      if (toTake === 0) {
        return []
      }
      let jobs = queuedInScope.get()
      let toLeave = R.max(0, jobs.length - toTake)
      let taken = R.take(toTake, jobs)

      runningInScope.concat(taken)
      queuedInScope.set(R.takeLast(toLeave, jobs))

      R.map(run, taken)

      return taken
    }
  }

  function run (job) {
    let internalJob = internalQueue[job.id]
    let delay = scopes.select(internalJob.scope).get().delay
    let timeoutDelay = scopes.select(internalJob.scope).get().timeout
    if (!R.isNil(timeoutDelay) && timeoutDelay !== 0 && timeoutDelay !== Infinity) {
      timeouts[job.id] = setTimeout(() => timeout(timeoutDelay, job.id), timeoutDelay * 1000)
    }
    internalJob.state = { pending: true }
    fork(job.id, internalJob.fn, internalJob.consumerResolve, internalJob.consumerReject, delay, internalJob.state)
    return internalJob.state
  }

  function purge (id) {
    let purged = internalQueue[id]
    let runningInScope = running.select(purged.scope)
    let stillRunning = R.reject(R.propEq('id', id), runningInScope.get())
    runningInScope.set(stillRunning)
    internalQueue = R.dissoc(id, internalQueue)
    if (!R.isNil(timeouts[id])) {
      clearTimeout(timeouts[id])
      timeouts = R.dissoc(id, timeouts)
    }
    process.nextTick(() => advance(purged.scope))
    return purged
  }

  function timeout (delay, id) {
    let internalJob = internalQueue[id]
    if (R.isNil(internalJob)) {
      return
    }
    let isCompleted = !internalJob.state.pending
    if (!isCompleted) {
      internalJob.consumerReject(err('The action timed out after ' + delay + ' seconds.'))
      timeouts = R.dissoc(id, timeouts)
    }
  }

  function scopeExists (name) {
    return scopes.exists(name)
  }

  function shutdown () {
    R.mapObjIndexed(clearTimeout, timeouts)
    R.mapObjIndexed((job, id) => internalQueue[id].consumerReject(err('Action cancelled - shutting down')), internalQueue)
  }

  return { enqueue: R.curry(enqueue), scopeExists, shutdown }
}
