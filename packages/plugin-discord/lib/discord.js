/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const DiscordBot = require('discord.js')

async function getClient (identity) {
  let discord = new DiscordBot.Client()
  await discord.login(identity.token)
  return {
    discord,
    identity,
    channels: {}
  }
}

async function getIdentity (client) {
  return client.identity
}

async function listenAct (api, cfg, msg) {
  if (R.isNil(msg)) {
    api.client.discord.on('message', R.curry(listenAct)(api, cfg))
    return
  }
  if (msg.author.bot) {
    return
  }
  let cmdexps = R.flatten(R.of(cfg.commandRegexp))
  for (let cmdregexp of cmdexps) {
    let cmdexp = T.mkRegExp(cmdregexp)
    if (!R.test(cmdexp, msg)) {
      continue
    }
    let parts = R.omit(['index', 'input'], cmdexp.exec(msg))
    let metadata = { channel: 'id' + msg.channel.id }
    for (let i in parts) {
      metadata['parameter' + i] = parts[i]
    }
    metadata = T.collapse('discord:', metadata)
    metadata.origin = 'discordListen'
    api.announce(metadata)
  }
}

async function sendAct (api, cfg, fact) {
  let applyTemplate = R.bind(fact, fact.applyTemplate)
  let templates = R.flatten(R.of(cfg.messages))
  let messages = R.map(applyTemplate, templates)
  let channelNames = R.map(applyTemplate, R.flatten(R.of(cfg.channel)))
  let targetChannels = R.map(R.prop(R.__, api.client.discord.channels), channelNames)
  for (let i in targetChannels) {
    let channel = targetChannels[i]
    let channelName = channelNames[i]
    if (R.isNil(channel)) {
      let found = api.client.discord.channels.find(c => c.name === channelName && c.type === 'text')
      if (R.isNil(found)) {
        continue
      }
      api.client.channels[channelName] = channel = found
    }
    for (let message of messages) {
      if (T.isMissing(message)) {
        api.enqueue(() => channel.sendMessage(message))
      }
    }
  }
}

const plugins = {
  discordListen: {
    type: 'input',
    requires: ['announce', 'indentity', 'activation'],
    sanity: {
      type: 'object',
      required: ['identity', 'commandRegexp'],
      properties: {
        commandRegexp: { type: ['string', 'array'] }
      }
    },
    limits: {
      upstream: 'discord',
      concurrency: 1,
      delay: 0.1,
      timeout: 60
    },
    act: listenAct,
    getClient,
    getIdentity
  },
  discordSend: {
    type: 'output',
    requires: ['identity'],
    sanity: {
      type: 'object',
      required: ['identity', 'channel', 'messages'],
      properties: {
        channel: { type: ['string', 'array'] },
        messages: { type: ['string', 'array'] }
      }
    },
    limits: {
      upstream: 'discord',
      concurrency: 1,
      delay: 0.1,
      timeout: 60
    },
    act: sendAct,
    getClient,
    getIdentity
  }
}

module.exports = plugins
