/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const path = require('path')
const _spawn = require('child_process').spawn
const T = require('@egeria/tools')

var ytdlProcesses = []

function delay (interval) {
  return new Promise((resolve) => {
    setTimeout(resolve, interval)
  })
}

function cleanOutput (d) {
  return R.replace(/\r?\n|\r/, '', d.toString())
}

const spawn = (cmd, args) => {
  return new Promise((resolve, reject) => {
    var proc = _spawn(cmd, args, { stdio: ['ignore', 'pipe', 'pipe'] })
    proc.done = false
    let errOutput = ''
    let stdOutput = ''
    proc.stderr.on('data', (d) => {
      errOutput += cleanOutput(d)
    })
    proc.stdout.on('data', (d) => {
      stdOutput += cleanOutput(d)
    })
    proc.on('error', (code) => {
      proc.done = true
      ytdlProcesses = R.reject(R.prop('done'), ytdlProcesses)
      var e = new Error('youtube-dl process terminated abnormally with code "' + code + '"')
      reject(e)
    })
    proc.on('close', (code, signal) => {
      proc.done = true
      ytdlProcesses = R.reject(R.prop('done'), ytdlProcesses)
      if (code !== 0 || signal === 'SIGTERM') {
        var reason = (R.isNil(signal) ? ' - exit code ' + code : ' - received signal ' + signal)
        var e = new Error('youtube-dl process terminated abnormally' + reason)
        reject(e)
      } else {
        resolve({ errOutput, stdOutput })
      }
    })
    ytdlProcesses.push(proc)
  })
}

async function act (api, cfg, fact) {
  let prefix = 'PLUGIN youtubeDownloader |'
  const cmd = R.defaultTo('youtube-dl', cfg.binary)
  let args = [
    '--no-progress',
    '--socket-timeout', '5',
    '--retries', '3',
    '--hls-prefer-native'
  ]
  if (!R.defaultTo(false, cfg.mtime)) {
    args.push('--no-mtime')
  }
  var format = cfg.format
  if (!T.isMissing(format)) {
    args.push('-f')
    args.push(format)
  }
  args.push('--merge-output-format')
  args.push(R.defaultTo('mkv', cfg.container))
  args.push('-o')
  const dldir = fact.applyTemplate(cfg.downloadDirectory)
  const link = fact.applyTemplate(cfg.link)
  let filename = fact.applyTemplate(cfg.filename)
  filename = T.sanitizeFilename(filename, '_')
  filename = R.replace('%', '%%', filename) // make sure youtube-dl doesn't get any unescaped "%"
  filename = filename + '.%(ext)s'
  const destination = path.resolve(dldir, filename)
  args.push(destination)
  args.push(link)
  const act = 'While downloading video from ' + link + ' to ' + destination
  try {
    await api.enqueue(() => spawn(cmd, args))
  } catch (e) {
    throw T.err(prefix, act, e, args)
  }
  return fact
}

const killall = () => {
  ytdlProcesses = R.reject(R.prop('done'), ytdlProcesses)
  R.map(R.invoker(1, 'kill')('SIGTERM'), ytdlProcesses)
}

async function shutdown (countdown) {
  while (ytdlProcesses.length > 0 && --countdown > 0) {
    killall()
    await delay(1000)
  }
}

async function destruct () {
  await shutdown(10)
}

const plugins = {
  youtubeDownloader: {
    type: 'output',
    requires: ['destruction'],
    sanity: {
      required: ['link', 'downloadDirectory', 'filename'],
      properties: {
        binary: { type: 'string' },
        link: { type: 'string' },
        downloadDirectory: { type: 'string' },
        filename: { type: 'string' },
        format: { type: 'string' },
        container: { type: 'string', enum: ['mkv', 'mp4', 'ogg', 'webm', 'flv'] },
        mtime: { type: 'boolean' }
      }
    },
    limits: {
      upstream: 'youtubedl',
      concurrency: 1,
      delay: 0.1,
      timeout: 21600
    },
    act,
    destruct
  }
}

module.exports = plugins
