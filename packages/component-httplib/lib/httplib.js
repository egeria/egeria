/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

module.exports = (function () {
  const R = require('ramda')
  const _request = require('request')
  const cloudscraper = require('cloudscraper')
  const cheerio = require('cheerio')
  const xray = require('x-ray')
  const FS = require('@egeria/fslib')
  const path = require('path')
  const url = require('url')

  function filenameFromURL (u) {
    return decodeURI(path.basename(url.parse(u).pathname))
  }

  const _defRequest = _request.defaults({
    jar: true,
    gzip: true,
    headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0' },
    /* followRedirect: function(response) {
      var T = require('egeria-tools');
      var url = require('url');
      var from = response.request.href;
      var to = url.resolve(response.headers.location, response.request.href);
      T.trace('redirected FROM ' + from);
      T.trace('redirected TO ' + response.headers.location);
      T.trace('request HEADERS ' + R.toString(response.request.headers));
      T.trace('response HEADERS ' + R.toString(response.headers));
      return true;
    }, */
    followAllRedirects: true
  })

  function request (opts) {
    return new Promise((resolve, reject) => {
      _defRequest(opts, (err, res) => {
        if (err) {
          reject(err)
        } else {
          resolve(res)
        }
      })
    })
  }

  async function http2hd (opts, target) {
    return new Promise((resolve, reject) => {
      let req = _defRequest(opts)
      req.on('error', reject)
      req.on('response', (res) => {
        res.on('end', resolve)
        res.pipe(FS.createWriteStream(target))
      })
    })
  }

  function cfrequest (opts) {
    if (!R.is(String, opts) && R.isNil(opts.method)) {
      opts.method = 'get'
    }
    return cloudscraper(opts)
  }

  function scrapeHTML (selector, attr, body) {
    var jq = cheerio.load(body)
    if (attr === 'text') {
      return jq(selector).map(function () { return jq(this).text() }).get()
    } else {
      return jq(selector).map(function () { return jq(this).attr(attr) }).get()
    }
  }

  function xscrape (body, selector, def) {
    let _x = xray()
    return new Promise((resolve, reject) => {
      _x(body, selector, def)((err, obj) => {
        if (!R.isNil(err)) {
          reject(err)
        } else {
          resolve(obj)
        }
      })
    })
  }

  const unarchiveOriginal = R.pipe(
    R.curry(scrapeHTML)('td:contains(Original) ~ td input', 'value'),
    R.head
  )

  const unarchiveFrom = R.pipe(
    R.curry(scrapeHTML)('td:contains(Saved from) ~ td input[type=text]', 'value'),
    R.head
  )

  const unwayback = R.pipe(
    R.curry(scrapeHTML)('#wmtbURL', 'value'),
    R.head,
    R.defaultTo('')
  )

  const ununvisit = R.pipe(
    R.curry(scrapeHTML)('a.perma', 'text'),
    R.head,
    R.defaultTo('')
  )

  const ungoogle = R.pipe(
    R.curry(scrapeHTML)('base', 'href'),
    R.head,
    R.defaultTo('')
  )

  async function process (retry, res) {
    let url = R.path(['request', 'uri', 'href'], res)
    if (R.test(/^https?:\/\/archive\.(is|fo|li|today|vn|md|ph)/i, url)) {
      let unarchived = unarchiveOriginal(res.body)
      if (R.either(R.isNil, R.isEmpty)(unarchived)) {
        unarchived = R.defaultTo('', unarchiveFrom(res.body))
      }
      if (unarchived === '') {
        throw new Error('Could not extract the original URL from archive.is (' + url + '); this is a bug. Giving up.')
      } else {
        return retry(unarchived)
      }
    } else if (R.test(/^https?:\/\/web.archive.org/i, url)) {
      const unwaybacked = unwayback(res.body)
      if (unwaybacked === '') {
        throw new Error('Could not extract the original URL from the wayback machine (' + url + '); this is a bug. Giving up.')
      } else {
        return retry(unwaybacked)
      }
    } else if (R.test(/^https?:\/\/unvis\.it/i, url)) {
      const ununvisited = ununvisit(res.body)
      if (ununvisited === '') {
        throw new Error('Could not extract the original URL from unvis.it (' + url + '); this is a bug. Giving up.')
      } else {
        return retry(ununvisited)
      }
    } else if (R.test(/^https?:\/\/webcache\.googleusercontent\.com/i, url)) {
      const ungoogled = ungoogle(res.body)
      if (ungoogled === '') {
        throw new Error('Could not extract the original URL from Google Cache (' + url + '); this is a bug. Giving up.')
      } else {
        return retry(ungoogled)
      }
    } else if (R.test(/store\.steampowered\.com\/agecheck/i, url)) {
      let formaction = scrapeHTML('#agecheck_form', 'action', res.body)[0]
      let snr = scrapeHTML('#agecheck_form input[name=snr]', 'value', res.body)[0]
      let parms = {
        method: 'post',
        uri: formaction,
        form: {
          ageDay: 1,
          ageMonth: 1,
          ageYear: 1980,
          snr: snr
        }
      }
      return request(parms)
    } else {
      return res
    }
  }

  async function httpreq (opts) {
    let res = await request(opts)
    let final = await process(httpreq, res)
    return final
  }

  async function cfhttpreq (opts) {
    let res = await cfrequest(opts)
    let final = await process(cfhttpreq, res)
    return final
  }

  async function download (opts, destination) {
    let parms = R.dissoc('preserveFilename', opts)
    let target = destination
    if (opts.preserveFilename) {
      FS.mkdir(target)
      let fileName = filenameFromURL(opts.uri)
      target = path.resolve(target, fileName)
    } else {
      FS.mkdir(path.dirname(target))
    }
    return http2hd(parms, target)
  }

  return { httpreq, cfhttpreq, scrapeHTML, xscrape, download, filenameFromURL }
})()
