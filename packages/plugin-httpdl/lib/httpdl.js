/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const W = require('@egeria/httplib')
const fs = require('fs')
const froid = require('froid')

const prefix = 'PLUGIN httpDownloader |'

const rethrow = R.curry((prefix, action, e) => {
  throw T.err(prefix, action, e)
})

function save (path, content) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, content, (err) => {
      if (!R.isNil(err)) {
        reject(err)
      } else {
        resolve(content)
      }
    })
  })
}

async function act (api, cfg = {}, fact) {
  let url = fact.applyTemplate(cfg.url)
  let tgtpath = fact.applyTemplate(cfg.path)
  let jar = fact.apply(R.prop(cfg.cookiejar))
  let sanity = { url: { complexType: 'URI' }, tgtpath: { type: String } }
  let err = froid(sanity, { url, tgtpath })
  if (!R.isNil(err)) {
    throw T.err(prefix, err)
  } else {
    let action = 'While downloading "' + url + '"'
    if (cfg.cloudflare) {
      let content
      try {
        content = await api.enqueue(() => W.cfhttpreq({
          method: 'get',
          uri: url,
          jar: jar || true
        }))
      } catch (e) {
        rethrow(prefix, action, e)
      }
      action = 'While saving "' + url + '" to "' + tgtpath + '"'
      let destination
      if (cfg.preserveFilename) {
        destination = require('path').resolve(tgtpath, W.filenameFromURL(url))
      } else {
        destination = tgtpath
      }
      try {
        await save(destination, content)
      } catch (e) {
        rethrow(prefix, action, e)
      }
    } else {
      let parms = {
        method: 'get',
        preserveFilename: cfg.preserveFileName,
        uri: url,
        jar: jar || true
      }
      try {
        await W.download(parms, tgtpath)
      } catch (e) {
        rethrow(prefix, action, e)
      }
    }
    return fact
  }
}

const plugins = {
  httpDownloader: {
    type: 'output',
    sanity: {
      type: 'object',
      required: ['url', 'path'],
      properties: {
        url: { type: 'string' },
        path: { type: 'string' },
        preserveFileName: { type: 'boolean' },
        cloudflare: { type: 'boolean' },
        cookiejar: { type: 'string' }
      }
    },
    limits: {
      upstream: 'httpdl',
      concurrency: 1,
      delay: 1,
      timeout: 60
    },
    act
  }
}

module.exports = plugins
