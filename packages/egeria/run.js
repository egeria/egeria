#!/usr/bin/env node

/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016 MySidesTheyAreGone

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

*/

const program = require('commander')
const path = require('path')
const defaultConfig = path.resolve(require('os').homedir(), '.egeria/config.js')

const die = (e) => {
  if (e) {
    console.log(e.message)
    console.log(e.stack)
  }
  console.log('TERMINATING')
  process.exit(e ? 1 : 0)
}

program
  .option('-c, --config [path]', 'Egeria\'s configuration file (default is ' + defaultConfig + ')')
  .parse(process.argv)

let config

try {
  config = require(program.config == null || program.config === '' ? defaultConfig : path.resolve(program.config))
} catch (e) {
  console.log('Couldn\'t load the configuration file.')
  die(e)
}

var Egeria = require('./lib/egeria.js')

async function run () {
  let app = await Egeria(config)
  process.once('SIGHUP', () => app.shutdown())
  process.once('SIGTERM', () => app.shutdown())
  process.once('SIGINT', () => app.shutdown())
}

run().catch((e) => {
  console.log(e.stack)
  process.exit(1)
})
