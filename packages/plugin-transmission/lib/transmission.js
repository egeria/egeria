/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const FS = require('@egeria/fslib')
const Powersteer = require('@egeria/powersteer')

const _torrentGetFields = [
  'activityDate', 'addedDate', 'bandwidthPriority', 'comment', 'corruptEver', 'creator', 'dateCreated', 'desiredAvailable', 'doneDate',
  'downloadDir', 'downloadedEver', 'downloadLimit', 'downloadLimited', 'error', 'errorString', 'eta', 'etaIdle', 'hashString',
  'haveUnchecked', 'haveValid', 'honorsSessionLimits', 'id', 'isFinished', 'isPrivate', 'isStalled', 'leftUntilDone', 'magnetLink', 'manualAnnounceTime',
  'maxConnectedPeers', 'metadataPercentComplete', 'name', 'peer-limit', 'peersConnected', 'peersGettingFromUs', 'peersSendingToUs',
  'percentDone', 'pieceCount', 'pieceSize', 'queuePosition', 'rateDownload', 'rateUpload', 'recheckProgress', 'secondsDownloading',
  'secondsSeeding', 'seedIdleLimit', 'seedIdleMode', 'seedRatioLimit', 'seedRatioMode', 'sizeWhenDone', 'startDate', 'status',
  'totalSize', 'torrentFile', 'uploadedEver', 'uploadLimit', 'uploadLimited', 'uploadRatio', 'webseedsSendingToUs'
]

async function getClient (identity) {
  let url = `http://${identity.host}:${identity.port}/transmission/rpc`
  if (T.isMissing(identity.password)) {
    return {
      identity,
      transmission: new Powersteer({ url })
    }
  } else {
    return {
      identity,
      transmission: new Powersteer({
        url,
        username: identity.username,
        password: identity.password
      })
    }
  }
}

async function getIdentity (client) {
  return client.identity
}

async function transmissionAddAction (api, cfg, fact) {
  let prefix = 'PLUGIN transmissionAdd |'
  let downloadDirectory = fact.applyTemplate(cfg.downloadDirectory)
  let torrent = fact.applyTemplate(cfg.torrent)
  if (T.isMissing(torrent)) {
    return fact
  }
  let args = {
    'download-dir': downloadDirectory,
    filename: torrent,
    paused: cfg.paused
  }
  let action = 'While trying to add a torrent to Transmission'
  try {
    await api.enqueue(() => api.client.transmission.torrentAdd(args))
    if (cfg.remove && torrent.indexOf('magnet:?') === -1 && torrent.indexOf('http://') === -1 && await FS.isFile(torrent)) {
      await FS.erase(torrent)
    }
  } catch (e) {
    throw T.err(prefix, action, e, args)
  }
  return fact
}

async function transmissionRemoveAction (api, cfg, fact) {
  let prefix = 'PLUGIN transmissionRemove |'
  var ids = R.map(R.bind(fact.applyTemplate, fact), R.flatten(R.of(cfg.id)))
  ids = R.reject(T.isMissing, ids)
  if (!R.isEmpty(ids)) {
    var args = {
      'delete-local-data': cfg.removeData,
      ids: ids
    }
    let action = 'While removing one or more torrents (ids: ' + R.join(', ', ids) + ')'
    try {
      await api.enqueue(() => api.client.transmission.torrentRemove(args))
    } catch (e) {
      throw T.err(prefix, action, e, args)
    }
  }
  return fact
}

async function transmissionListAction (api, cfg) {
  let prefix = 'PLUGIN transmissionList |'
  let action = `While fetching the list of torrents from ${api.client.identity.host}:${api.client.identity.port}`
  let listResponse
  try {
    listResponse = await api.client.transmission.torrentGet({ fields: _torrentGetFields })
  } catch (e) {
    throw T.err(prefix, action, e, { method: 'torrentGet', args: { fields: _torrentGetFields } })
  }
  let torrents = R.path(['arguments', 'torrents'], listResponse)
  if (!T.isMissing(torrents)) {
    for (let torrent of torrents) {
      torrent.key = 'transmission:' + torrent.hashString
      torrent = T.collapse('transmission:', torrent)
      torrent.key = torrent['transmission:key']
      torrent.origin = 'transmissionList'
      api.announce(torrent)
    }
  }
}

const limits = {
  upstream: 'transmission',
  concurrency: 1,
  delay: 1,
  timeout: 120
}

const plugins = {
  transmissionAdd: {
    type: 'output',
    requires: ['identity'],
    sanity: {
      required: ['torrent', 'downloadDirectory'],
      properties: {
        torrent: { type: 'string' },
        downloadDirectory: { type: 'string' },
        paused: { type: 'boolean' },
        remove: { type: 'boolean' }
      }
    },
    limits,
    getClient,
    getIdentity,
    act: transmissionAddAction
  },
  transmissionRemove: {
    type: 'output',
    requires: ['identity'],
    sanity: {
      required: ['id'],
      properties: {
        id: { type: ['string', 'array'] },
        removeData: { type: 'boolean' }
      }
    },
    limits,
    getClient,
    getIdentity,
    act: transmissionRemoveAction
  },
  transmissionList: {
    type: 'input',
    paradigm: 'service',
    requires: ['identity', 'schedule'],
    sanity: {},
    limits,
    getClient,
    getIdentity,
    act: transmissionListAction
  }
}

module.exports = plugins
