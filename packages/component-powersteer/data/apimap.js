const R = require('ramda')

const torrentGetFields = [
  'activityDate', 'addedDate', 'bandwidthPriority', 'comment', 'corruptEver', 'creator', 'dateCreated', 'desiredAvailable', 'doneDate',
  'downloadDir', 'downloadedEver', 'downloadLimit', 'downloadLimited', 'error', 'errorString', 'eta', 'etaIdle', 'files', 'fileStats', 'hashString',
  'haveUnchecked', 'haveValid', 'honorsSessionLimits', 'id', 'isFinished', 'isPrivate', 'isStalled', 'leftUntilDone', 'magnetLink', 'manualAnnounceTime',
  'maxConnectedPeers', 'metadataPercentComplete', 'name', 'peer-limit', 'peers', 'peersConnected', 'peersFrom', 'peersGettingFromUs', 'peersSendingToUs',
  'percentDone', 'pieces', 'pieceCount', 'pieceSize', 'priorities', 'queuePosition', 'rateDownload', 'rateUpload', 'recheckProgress', 'secondsDownloading',
  'secondsSeeding', 'seedIdleLimit', 'seedIdleMode', 'seedRatioLimit', 'seedRatioMode', 'sizeWhenDone', 'startDate', 'status', 'trackers', 'trackerStats',
  'totalSize', 'torrentFile', 'uploadedEver', 'uploadLimit', 'uploadLimited', 'uploadRatio', 'wanted', 'webseeds', 'webseedsSendingToUs'
]

const torrentSetArgs = {
  'bandwidthPriority': Number,
  'downloadLimit': Number,
  'downloadLimited': Boolean,
  'files-wanted': Array,
  'files-unwanted': Array,
  'honorsSessionLimits': Boolean,
  'ids': Array,
  'location': String,
  'peer-limit': Number,
  'priority-high': Array,
  'priority-low': Array,
  'priority-normal': Array,
  'queuePosition': Number,
  'seedIdleLimit': Number,
  'seedIdleMode': Number,
  'seedRatioLimit': Number,
  'seedRatioMode': Number,
  'trackerAdd': Array,
  'trackerRemove': Array,
  'trackerReplace': Array,
  'uploadLimit': Number,
  'uploadLimited': Boolean
}

const torrentAddArgs = {
  'cookies': String,
  'download-dir': String,
  'filename': String,
  'metainfo': String,
  'paused': Boolean,
  'peer-limit': Number,
  'bandwidthPriority': Number,
  'files-wanted': Array,
  'files-unwanted': Array,
  'priority-high': Array,
  'priority-low': Array,
  'priority-normal': Array }

const sessionSetArgs = {
  'alt-speed-down': Number,
  'alt-speed-enabled': Boolean,
  'alt-speed-time-begin': Number,
  'alt-speed-time-enabled': Boolean,
  'alt-speed-time-end': Number,
  'alt-speed-time-day': Number,
  'alt-speed-up': Number,
  'blocklist-url': String,
  'blocklist-enabled': Boolean,
  'blocklist-size': Number,
  'cache-size-mb': Number,
  'config-dir': String,
  'download-dir': String,
  'download-queue-size': Number,
  'download-queue-enabled': Boolean,
  'dht-enabled': Boolean,
  'encryption': String,
  'idle-seeding-limit': Number,
  'idle-seeding-limit-enabled': Boolean,
  'incomplete-dir': String,
  'incomplete-dir-enabled': Boolean,
  'lpd-enabled': Boolean,
  'peer-limit-global': Number,
  'peer-limit-per-torrent': Number,
  'pex-enabled': Boolean,
  'peer-port': Number,
  'peer-port-random-on-start': Boolean,
  'port-forwarding-enabled': Boolean,
  'queue-stalled-enabled': Boolean,
  'queue-stalled-minutes': Number,
  'rename-partial-files': Boolean,
  'rpc-version': Number,
  'rpc-version-minimum': Number,
  'script-torrent-done-filename': String,
  'script-torrent-done-enabled': Boolean,
  'seedRatioLimit': Number,
  'seedRatioLimited': Boolean,
  'seed-queue-size': Number,
  'seed-queue-enabled': Boolean,
  'speed-limit-down': Number,
  'speed-limit-down-enabled': Boolean,
  'speed-limit-up': Number,
  'speed-limit-up-enabled': Boolean,
  'start-added-torrents': Boolean,
  'trash-original-torrent-files': Boolean,
  'utp-enabled': Boolean,
  'version': String
}

let apimap = {
  sessionField: 'x-transmission-session-id',
  methods: {
    torrentStart: 'torrent-start',
    torrentStartNow: 'torrent-start-now',
    torrentStop: 'torrent-stop',
    torrentVerify: 'torrent-verify',
    torrentReannounce: 'torrent-reannounce',
    torrentSet: 'torrent-set',
    torrentGet: 'torrent-get',
    torrentAdd: 'torrent-add',
    torrentRemove: 'torrent-remove',
    torrentSetLocation: 'torrent-set-location',
    torrentRenamePath: 'torrent-rename-path',
    sessionSet: 'session-set',
    sessionGet: 'session-get',
    sessionStats: 'session-stats',
    blocklistUpdate: 'blocklist-update',
    portTest: 'port-test',
    sessionClose: 'session-close',
    queueMoveTop: 'queue-move-top',
    queueMoveUp: 'queue-move-up',
    queueMoveDown: 'queue-move-down',
    queueMoveBottom: 'queue-move-bottom',
    freeSpace: 'free-space'
  },
  sanity: {
    torrentGet: {
      fields: { type: Array, anyOf: torrentGetFields, mandatory: true },
      ids: { Array }
    },
    torrentSet: R.mapObjIndexed((type) => ({ type: type }), torrentSetArgs),
    torrentAdd: R.mapObjIndexed((type) => ({ type: type }), torrentAddArgs),
    sessionSet: R.merge(
      R.mapObjIndexed((type) => ({ type: type }), sessionSetArgs),
      { allowed: R.concat(R.keys(sessionSetArgs, ['units'])) }
    ),
    torrentRemove: {
      ids: { type: Array },
      'delete-local-data': { type: Boolean }
    },
    torrentSetLocation: {
      ids: { type: Array },
      location: { type: String },
      move: { type: Boolean }
    },
    torrentRenamePath: {
      ids: { type: Array },
      path: { type: String },
      name: { type: String }
    },
    freeSpace: {
      path: { type: String, mandatory: true }
    }
  }
}

// calls that expect no arguments
const _sanityNoneAllowed = { allowed: [] }
R.forEach((call) => (apimap.sanity[call] = _sanityNoneAllowed), [
  'sessionGet',
  'sessionStats',
  'sessionClose',
  'blocklistUpdate',
  'portTest'
])

// calls that only expect ids
const _sanityIdsAllowed = { ids: { type: Array }, allowed: ['ids'] }
R.forEach((call) => (apimap.sanity[call] = _sanityIdsAllowed), [
  'torrentStart',
  'torrentStartNow',
  'torrentStop',
  'torrentVerify',
  'torrentReannounce'
])

// queue operations
const _sanityOnlyIdsRequired = { ids: { type: Array, mandatory: true }, allowed: ['ids'] }
R.forEach((call) => (apimap.sanity[call] = _sanityOnlyIdsRequired), [
  'queueMoveTop',
  'queueMoveUp',
  'queueMoveDown',
  'queueMoveBottom'
])

// allowed same as type
R.forEach((call) => (apimap.sanity[call].allowed = R.keys(apimap.sanity[call].type)), [
  'torrentGet',
  'torrentSet',
  'torrentAdd',
  'torrentRemove',
  'torrentSetLocation',
  'torrentRenamePath'
])

module.exports = apimap
