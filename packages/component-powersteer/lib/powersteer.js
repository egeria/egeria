/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const _request = require('request')
const froid = require('froid')
const EError = require('@egeria/error')
const apimap = require('../data/apimap.js')

const prefix = 'Powersteer |'

function request (opts) {
  return new Promise((resolve, reject) => _request(opts, (err, res) => {
    if (!R.isNil(err)) {
      reject(err)
    } else {
      resolve(res)
    }
  }))
}

async function query (obj, call, req) {
  let action = 'While performing API call "' + call + '"'
  let res
  try {
    res = await request(req)
  } catch (err) {
    throw new EError(prefix, action, err, req)
  }
  if (res.headers[apimap.sessionField]) {
    obj.headers[apimap.sessionField] = req.headers[apimap.sessionField] = res.headers[apimap.sessionField]
  }
  if (res.statusCode === 401) {
    throw new EError(prefix, action, new Error('401: Invalid username/password'), req)
  }
  if (res.statusCode === 409) {
    // wrong session ID
    // the session ID has already been updated, so just repeat the call
    res = await query(obj, call, req)
  }
  return res
}

function parse (call, req, res) {
  let action = 'While parsing response of API call "' + call + '"'
  let body
  try {
    body = JSON.parse(res.body)
  } catch (e) {
    throw new EError(prefix, action, e, req)
  }
  if (body.result !== 'success') {
    throw new EError(prefix, action, new Error('Unsuccessful: ' + body.result), req)
  } else {
    return body
  }
}

async function apiCall (obj, call, args) {
  let payload = JSON.stringify({
    'method': call,
    'arguments': args
  })
  let req = { uri: obj.url, body: payload, headers: obj.headers, method: 'post' }
  let response = await query(obj, call, req)
  return parse(call, req, response)
}

function Powersteer (options) {
  let config = options || {}

  let sanity = {
    url: { complexType: 'URI', mandatory: true },
    username: { type: String, depends: 'password' },
    password: { type: String, depends: 'username' }
  }

  let errorMsg = froid(sanity, config)
  if (!R.isNil(errorMsg)) {
    throw new EError(prefix, errorMsg)
  }

  this.url = config.url
  this.headers = { 'Content-Type': 'application/json' }
  if (!R.isNil(config.username)) {
    this.username = config.username
    this.password = config.password
    let auth = Buffer.from(this.username + ':' + this.password).toString('base64')
    this.headers['Authorization'] = 'Basic ' + auth
  }
}

Powersteer.prototype = R.mapObjIndexed((call, method) => {
  return function (args) {
    let options = args || {}

    let errorMsg = froid(R.dissoc('allowed', apimap.sanity[method]), options)
    if (!R.isNil(errorMsg)) {
      throw new EError(prefix, errorMsg)
    }

    let exclude = R.omit(apimap.sanity[method].allowed, options)
    options = R.omit(R.keys(exclude), options)

    return apiCall(this, call, args)
  }
}, apimap.methods)

module.exports = Powersteer
