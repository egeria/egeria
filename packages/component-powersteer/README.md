# [![Egeria](https://gitlab.com/egeria/powersteer/raw/master/logo/logo_small.png)](https://gitlab.com/egeria/powersteer) Powersteer

[![NPM version](http://img.shields.io/npm/v/powersteer.svg)](https://www.npmjs.com/package/powersteer)
[![Downloads](https://img.shields.io/npm/dm/powersteer.svg)](https://www.npmjs.com/package/powersteer)

## About

Powersteer lets you communicate with Transmission.

It provides access to the entire Transmission API and performs checks on arguments before sending anything to transmission-daemon.


## Creating a new client

Require Powersteer and create a new client:

```js
var Powersteer = require('powersteer');

var rpc = new Powersteer({
    url: 'http://my.box.ip:9091/transmission/rpc',
    username: 'mysides',
    password: 'theyaregone'
});
```

The constructor accepts an object with the following fields:

### `url` (mandatory)

Self-explanatory enough, but make sure you don't simply give the ip and port of your box; Transmission listens for RPC calls on a specific HTTP address. If we assume the Transmission instance to be installed locally, the default address is 'http://127.0.0.1:9091/transmission/rpc'.

### `username` (optional)

If you ave secured your Transmission daemon, put your username here.

### `password` (optional)

If you have provided a username, the password becomes mandatory. The constructor will throw if you don't provide one.


If you set one of the fields `username` and `password` to any value that isn't `null`, the other field becomes mandatory.


## Using a client

I tried to follow Transmission's [docs](https://trac.transmissionbt.com/browser/trunk/extras/rpc-spec.txt) as closely as possible.

These are the methods available in Powersteer with the respective Transmission procedures:

|Powersteer|Transmission|
|---|---|
|torrentStart | 'torrent-start'|
|torrentStartNow | 'torrent-start-now'|
|torrentStop | 'torrent-stop'|
|torrentVerify | 'torrent-verify'|
|torrentReannounce | 'torrent-reannounce'|
|torrentSet | 'torrent-set'|
|torrentGet | 'torrent-get'|
|torrentAdd | 'torrent-add'|
|torrentRemove | 'torrent-remove'|
|torrentSetLocation | 'torrent-set-location'|
|torrentRenamePath | 'torrent-rename-path'|
|sessionSet | 'session-set'|
|sessionGet | 'session-get'|
|sessionStats | 'session-stats'|
|blocklistUpdate | 'blocklist-update'|
|portTest | 'port-test'|
|sessionClose | 'session-close'|
|queueMoveTop | 'queue-move-top'|
|queueMoveUp | 'queue-move-up'|
|queueMoveDown | 'queue-move-down'|
|queueMoveBottom | 'queue-move-bottom'|
|freeSpace | 'free-space'|



### `torrentStart`, `torrentStartNow`, `torrentStop`, `torrentVerify`, `torrentReannounce`

Arguments: a list of torrent ids in the form:

```js
{
    ids: [1, 2, 3, ...]
}
```

### `torrentSet`

Arguments:

|Field|Type|Description|
|---|---|---|
|bandwidthPriority   | Number  |  this torrent's bandwidth tr_priority_t|
|downloadLimit       | Number  |  maximum download speed (KBps)|
|downloadLimited     | Boolean |  true if downloadLimit is honored|
|files-wanted        | Array   |  indices of file(s) to download|
|files-unwanted      | Array   |  indices of file(s) to not download|
|honorsSessionLimits | Boolean |  true if session upload limits are honored|
|ids                 | Array   |  list of torrent ids; if empty, it means "all torrents"|
|location            | String  |  new location of the torrent's content|
|peer-limit          | Number  |  maximum number of peers|
|priority-high       | Array   |  indices of high-priority file(s)|
|priority-low        | Array   |  indices of low-priority file(s)|
|priority-normal     | Array   |  indices of normal-priority file(s)|
|queuePosition       | Number  |  position of this torrent in its queue [0...n)|
|seedIdleLimit       | Number  |  torrent-level number of minutes of seeding inactivity|
|seedIdleMode        | Number  |  which seeding inactivity to use.  See tr_idlelimit|
|seedRatioLimit      | Number  |  torrent-level seeding ratio|
|seedRatioMode       | Number  |  which ratio to use.  See tr_ratiolimit|
|trackerAdd          | Array   |  strings of announce URLs to add|
|trackerRemove       | Array   |  ids of trackers to remove|
|trackerReplace      | Array   |  pairs of <trackerId/new announce URLs>|
|uploadLimit         | Number  |  maximum upload speed (KBps)|
|uploadLimited       | Boolean |  true if uploadLimit is honored|

You must specify at least one.

### `torrentGet`

Arguments: a list of fields to retrieve in the form:

```js
{
    fields: ['field1', 'field2', 'field3', ...]
}
```

You must be precise. The correct name of every available field is in the [docs](https://trac.transmissionbt.com/browser/trunk/extras/rpc-spec.txt), section "3.3".

### `torrentAdd`

Arguments:

|Field                | Type     | Description|
|---|---|---|
|cookies            | String   |  pointer to a string of one or more cookies.|
|download-dir       | String   |  path to download the torrent to|
|filename           | String   |  filename or URL of the .torrent file|
|metainfo           | String   |  base64-encoded .torrent content|
|paused             | Boolean  |  if true, don't start the torrent|
|peer-limit         | Number   |  maximum number of peers|
|bandwidthPriority  | Number   |  torrent's bandwidth tr_priority_t|
|files-wanted       | Array    |  indices of file(s) to download|
|files-unwanted     | Array    |  indices of file(s) to not download|
|priority-high      | Array    |  indices of high-priority file(s)|
|priority-low       | Array    |  indices of low-priority file(s)|
|priority-normal    | Array    |  indices of normal-priority file(s)|


### `torrentRemove`

Arguments: a list of torrent ids and a Boolean `delete-local-data` field (when true, delete all downloaded files belonging to the removed torrents).


### `torrentSetLocation`

Arguments:

|Field|Type|Description|
|---|---|---|
|ids                            | Array     | torrent list
|location                       | String    | the new download location
|move                           | Boolean   | if true, move from previous location. otherwise, search "location" for files (default: false)


### `torrentRenamePath`

See the [docs](https://trac.transmissionbt.com/browser/trunk/extras/rpc-spec.txt), section "3.7".



### TODO (available but undocumented)

sessionSet, sessionGet, sessionStats, blocklistUpdate, portTest, sessionClose, queueMoveTop, queueMoveUp, queueMoveDown, queueMoveBottom, freeSpace

See the [docs](https://trac.transmissionbt.com/browser/trunk/extras/rpc-spec.txt)

## Example

```js
var Powersteer = require('powersteer');

var trace = (x) => {console.log('TRACE: ', x); return x;};

var rpc = new Powersteer({url: 'http://my.box.ip:9091/transmission/rpc'});

rpc.torrentGet({fields: ['id', 'name', 'percentDone']}).then(trace).catch(trace);

rpc.torrentAdd({filename: 'http://cdimage.debian.org/debian-cd/8.2.0/amd64/bt-cd/debian-8.2.0-amd64-CD-1.iso.torrent', paused: true}).then(trace).catch(trace);

```
