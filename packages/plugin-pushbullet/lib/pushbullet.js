/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Egeria - She bestows Knowledge and Wisdom
    Copyright (C) 2016-2019 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

const R = require('ramda')
const T = require('@egeria/tools')
const Pushbullet = require('pushbullet')

const rethrow = R.curry((prefix, action, e) => {
  throw T.err(prefix, action, e)
})

async function getClient (identity) {
  let client = {
    pushbullet: new Pushbullet(identity.key),
    stream: null,
    active: false,
    channelIds: {},
    after: null,
    identity
  }
  return client
}

async function getIdentity (client) {
  return client.identity
}

function getChannelInfo (api, tag) {
  return new Promise((resolve, reject) => {
    api.client.pushbullet.channelInfo(tag, (err, res) => {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

function getHistory (api) {
  return new Promise((resolve, reject) => {
    api.client.pushbullet.history({
      limit: 10,
      modified_after: api.client.after
    }, (err, res) => {
      if (err) {
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

function send (api, mode, target, title, body) {
  return new Promise((resolve, reject) => api.client.pushbullet[mode](target, title, body, (err) => {
    if (err) {
      reject(err)
    } else {
      resolve(true)
    }
  }))
}

async function getLatestPushTimestamp (api, enqueue) {
  let data = await enqueue(() => getHistory(api))
  let last = R.defaultTo(0, R.path(['pushes', 0, 'modified'], data))
  return last
}

async function getChannelIden (api, enqueue, tag) {
  let info = await enqueue(() => getChannelInfo(api.client.pushbullet, tag))
  return R.prop('iden', info)
}

async function sendAct (api, cfg, fact) {
  let action = 'While pushing a message'
  let title = fact.applyTemplate(cfg.title)
  let body = fact.applyTemplate(cfg.body)
  let target = {}
  if (!R.isNil(cfg.tag)) {
    target.channel_tag = cfg.tag
  }
  try {
    await api.enqueue(() => send(api, cfg.mode, target, title, body))
  } catch (e) {
    rethrow('PLUGIN pushbulletSend |', action, e)
  }
  return fact
}

async function listenAct (api, cfg, push) {
  if (T.isMissing(push)) {
    if (!R.isNil(cfg.tag)) {
      api.client.channelIds[cfg.tag] = await getChannelIden(api, cfg.tag)
    }
    api.client.after = await getLatestPushTimestamp(api)
    api.client.pushbullet.stream.on('message', R.curry(listenAct)(api, cfg))
    if (cfg.active) {
      api.client.active = true
      api.client.stream = api.client.pushbullet.stream()
      api.client.stream.connect()
      await new Promise((resolve, reject) => {
        api.client.stream.on('connect', resolve)
        api.client.stream.on('error', reject)
      })
    }
    return
  }
  if (push.subtype !== 'push') {
    return
  }
  let channelId = api.client.channelIds[cfg.tag]
  let history = await getHistory(api)
  if (history.pushes.length > 0) {
    api.client.after = R.head(history.pushes).modified
    if (!R.isNil(cfg.tag)) {
      history.pushes = R.filter(R.propEq('channel_iden', channelId), history.pushes)
    }
    for (let push of history.pushes) {
      if (!T.isMissing(cfg.commandRegexp)) {
        for (let cmdexp of cfg.commandRegexp) {
          let parts = R.match(T.mkRegExp(cmdexp), push.body)
          if (!T.isMissing(parts)) {
            push.parameter = R.tail(parts)
          }
        }
      }
      let metadata = T.collapse('pushbullet:', push)
      metadata.origin = 'pushbulletListen'
      api.announce(metadata)
    }
  }
}

const plugins = {
  pushbulletListen: {
    type: 'input',
    requires: ['identity', 'announce', 'activation'],
    sanity: {},
    limits: {
      upstream: 'pushbullet',
      concurrency: 1,
      delay: 0,
      timeout: 0
    },
    act: listenAct,
    getClient,
    getIdentity
  },
  pushbulletSend: {
    type: 'output',
    requires: ['identity'],
    sanity: {
      type: 'object',
      required: ['mode', 'body', 'identity'],
      properties: {
        mode: { type: 'string', enum: ['link', 'note'] },
        title: { type: 'string' },
        body: { type: 'string' },
        tag: { type: 'string' }
      }
    },
    limits: {
      upstream: 'pushbullet',
      concurrency: 1,
      delay: 3,
      timeout: 60
    },
    act: sendAct,
    getClient,
    getIdentity
  }
}

module.exports = plugins
