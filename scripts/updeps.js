import R from 'ramda'
import EFS from '@egeria/fslib'
import W from '@egeria/httplib'
import pathlib from 'path'
import semver from 'semver'

let masterPackageData = require(pathlib.resolve(__dirname, '../package.json'))

let cfg = {
  logLevels: {
    silly: true,
    debug: true,
    info: true,
    warning: true,
    error: true
  },
  packagedir: pathlib.resolve(__dirname, '../packages')
}

let cachedModuleVersions = {}

const isMissing = R.either(R.isNil, R.isEmpty)

const log = R.curry(function (level, message) {
  if (cfg.logLevels[level]) {
    console.log(`[${level}] ${message}`)
  }
})

function delay (n) {
  return new Promise(resolve => setTimeout(resolve, n * 1000))
}

function isAnUpgrade (v1, v2) {
  let result = false
  try {
    result = semver.gt(v1, v2)
  } catch (e) {
    log('warning', `Something went wrong comparing "${v1}" with "${v2}"`)
    log('warning', e.message)
  }
  return result
}

async function treemap (fileFilter, directoryFilter, fn, root) {
  for (let branch of await EFS.readdir(root)) {
    let path = pathlib.resolve(root, branch)
    if (await EFS.isFile(path) && fileFilter(path)) {
      await fn(root, branch)
    } else if (await EFS.isDirectory(path) && directoryFilter(path)) {
      await treemap(fileFilter, directoryFilter, fn, path)
    }
  }
}

async function getLatestVersion (module) {
  if (!isMissing(cachedModuleVersions[module])) {
    return cachedModuleVersions[module]
  }
  let { body } = await W.httpreq({ uri: 'https://registry.npmjs.org/' + encodeURIComponent(module).replace('%40', '@'), json: true })
  await delay(1)
  let latest = body['dist-tags']['latest']
  cachedModuleVersions[module] = latest
  return latest
}

async function updateDependencies (depObject) {
  if (isMissing(depObject)) {
    return null
  }
  let changed = false
  for (let dep of R.keys(depObject)) {
    let current = R.replace(/^[^0-9]/, '', depObject[dep])
    let latest = await getLatestVersion(dep)
    if (semver.gt(latest, current)) {
      log('info', `Upgrading ${dep} v${current} -> v${latest}`)
      changed = true
      depObject[dep] = latest
    }
  }
  return changed
}

async function process (moduleFilter, root, branch) {
  let pkgpath = pathlib.resolve(root, branch)
  let packageData = require(pkgpath)
  let updated = false
  for (let dep of R.keys(packageData.dependencies)) {
    if (!moduleFilter(dep)) {
      continue
    }
    packageData.dependencies[dep] = R.replace(/^[^0-9]/, '', packageData.dependencies[dep])
    if (isMissing(masterPackageData.managedDependencies[dep])) {
      log('error', `[${packageData.name}] Untracked dependency: ${dep} v${packageData.dependencies[dep]}`)
      masterPackageData.managedDependencies[dep] = packageData.dependencies[dep]
    }
    if (isAnUpgrade(masterPackageData.managedDependencies[dep], packageData.dependencies[dep])) {
      log('info', `[${packageData.name}] Updating dependency: ${dep} v${packageData.dependencies[dep]} -> v${masterPackageData.managedDependencies[dep]}`)
      packageData.dependencies[dep] = masterPackageData.managedDependencies[dep]
      updated = true
    }
  }
  if (updated) {
    log('info', `Writing ${pkgpath}...`)
    await EFS.writeFile(pkgpath, JSON.stringify(packageData, null, 2))
  }
}

const fileFilter = R.test(/package.json/)

const directoryFilter = R.pipe(R.test(/node_modules/), R.not)

const moduleFilter = R.always(true) // R.pipe(R.test(/@egeria/), R.not)

async function main () {
  log('info', `Checking managed dependencies...`)
  let masterPackageUpdated = await updateDependencies(masterPackageData.managedDependencies) && await updateDependencies(masterPackageData.devDependencies)
  if (masterPackageUpdated) {
    await EFS.writeFile(pathlib.resolve(__dirname, '../package.json'), JSON.stringify(masterPackageData, null, 2))
  }
  log('info', `Checking packages...`)
  await treemap(fileFilter, directoryFilter, R.curry(process)(moduleFilter), cfg.packagedir)
  await EFS.writeFile(pathlib.resolve(__dirname, '../package.json'), JSON.stringify(masterPackageData, null, 2))
  return 'OK'
}

main().then(console.log).catch(console.log)
